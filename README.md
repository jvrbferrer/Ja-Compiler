A front end compiler of a subset of the programming language Java called Ja.

To run install bison and flex.
Compile with:
lex jac.l
yacc jac.y
gcc code_generator.c lex.yy.c symbol_table.c tree.c y.tab.c -o out

The Ja program is feeded to the compiler through stdin.
The errors and generated llvm code will be printed to stdout.
