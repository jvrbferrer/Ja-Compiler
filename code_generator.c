#include "tree.h"

extern int is_error;


int cur_string_lit;
int cur_string_lit;
int cur_temp_var;
int last_used_label, last_reserved_label;

void generate_code(node *root, class_environment *class_env) {
    //FOR THE PRINTS WE NEED STATIC STRINGS AND A DEC AND A FLOAT FORMATTED STRING
    printf("@"STRING_CONST_PREFIX"dec = private unnamed_addr constant [4 x i8] c\"%%d\\0A\\00\"\n");
    printf("@"STRING_CONST_PREFIX"double = private unnamed_addr constant [7 x i8] c\"%%.16E""\\0A\\00\"\n");
    printf("@"STRING_CONST_PREFIX"bools = private unnamed_addr constant[13 x i8] c\"false\\0A\\00true\\0A\\00\"\n");
    cur_string_lit = 0;
    search_static_strings(root);
    printf("\n");
    cur_string_lit = 0;
    generate_class_vars(root, class_env);
    printf("\ndeclare i32 @printf(i8*, ...) nounwind\n");
    printf("declare i32 @atoi(i8*) #1\n");
    printf("\ndefine i32 @main(i32 %%argc, i8** %%argv) {\ncall void @"FUNCTION_PREFIX"main.StringArray(i32 %%argc, i8** %%argv)\nret i32 0\n}\n\n");

    int i;
    for(i=0; i<root->n_child; i++) {
        node *child = root->child[i];
        if(child == NULL) {
            continue;
        }
        else if(strcmp(root->child[i]->name, "MethodDecl") == 0) {
            cur_temp_var = -1;
            last_reserved_label = last_used_label = 0;
            generate_function(root->child[i]);
        }
    }
}

char *get_param_decl_name(node *method_params, int index) {
    int aux = 0, args_found = 0;

    while(args_found != index) {
        if(method_params->child[aux] == NULL) {
            aux++;
        }
        else {
            args_found++;
            aux++;
        }
    }

    return method_params->child[aux]->child[1]->value;
}

void generate_function(node *method_decl) {
    char *name = get_method_name(method_decl->child[0]);
    int n_args = get_method_n_args(method_decl->child[0]);
    char *return_type = get_method_return_type(method_decl->child[0]);
    char **args = get_method_args(method_decl->child[0], n_args);

    printf("define %s @"FUNCTION_PREFIX"%s", get_llvm_type(return_type), name);
    int i;
    for(i=0; i<n_args; i++) {
        printf(".%s", args[i]);
    }
    printf("(");
    for(i=0; i<n_args; i++) {
        if(strcmp(args[i], "StringArray") == 0) {
            printf("i32 %%"ARGS_PREFIX"argc, i8** %%"ARGS_PREFIX"argv"); //MEANS THAT STRING[] WILL ALWAYS BE CALLED argc argv.
            break;
        }
        if(i != 0) {
            printf(", ");
        }
        printf("%s %%%s", get_llvm_type(args[i]), get_param_decl_name(method_decl->child[0]-> child[2], i));
    }
    printf(") {\n");
    printf(LABEL_PREFIX"0:\n");

    for(i=0; i<n_args; i++) {
        if(strcmp(args[i], "StringArray") == 0) {
            break;
        }
        printf("%%%d = alloca %s\n", ++cur_temp_var, get_llvm_type(args[i]));
        printf("store %s %%%s, %s* %%%d\n", get_llvm_type(args[i]), get_param_decl_name(method_decl->child[0]-> child[2], i), get_llvm_type(args[i]), cur_temp_var);
    }

    node *method_body = method_decl->child[1];
    for(i=0; i<method_body->n_child; i++) {
        node *cur_child = method_body->child[i];
        get_proper_gen_function(cur_child->name)(cur_child);
    }
    printf("ret %s ", get_llvm_type(return_type));
    if(strcmp(return_type, "Int") == 0) {
        printf("0");
    }
    else if(strcmp(return_type, "Double") == 0) {
        printf("0.0");
    }
    else if(strcmp(return_type, "Bool") == 0) {
        printf("true");
    }
    printf("\n}\n\n");

}

void generate_print(node *print) {
    get_proper_gen_function(print->child[0]->name)(print->child[0]);
    if(strcmp(print->child[0]->anotation, "Int") == 0) {
        printf("call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @str.dec, i32 0, i32 0), i32 %%%d)\n", cur_temp_var++);
    }
    else if(strcmp(print->child[0]->anotation, "Double") == 0) {
        printf("call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @str.double, i32 0, i32 0), double %%%d)\n", cur_temp_var++);
    }
    else if(strcmp(print->child[0]->anotation, "String") == 0) {
        printf("call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([%d x i8], [%d x i8]* @str.%d, i32 0, i32 0))\n",print_as_llvm_string(print->child[0]->value, 0)+2, print_as_llvm_string(print->child[0]->value, 0)+2, cur_string_lit++);
        cur_temp_var++;
    }
    else if(strcmp(print->child[0]->anotation, "Bool") == 0) {
        printf("%%%d = zext i1 %%%d to i32 ;extends and fills with 0s\n", cur_temp_var+1, cur_temp_var);
        cur_temp_var++;
        printf("%%%d = mul i32 7, %%%d\n", cur_temp_var+1, cur_temp_var);
        cur_temp_var++;
        printf("%%%d = getelementptr inbounds [13 x i8], [13 x i8]* @str.bools, i32 0, i32 %%%d\n", cur_temp_var+1, cur_temp_var); cur_temp_var++;
        printf("call i32 (i8*, ...) @printf(i8* %%%d)\n", cur_temp_var++);
    }
}

void  remove_underscores(char *str) {

    int pos2 = 0, pos = 0;

    while(str[pos2] != '\0') {
        if(str[pos2] == '_') {
            pos2++;
            continue;
        }
        str[pos] = str[pos2];
        pos++; pos2++;
    }
    str[pos] = '\0';
}

void generate_declit(node *declit) {

    remove_underscores(declit->value);
    printf("%%%d = add i32 0, %s\n", ++cur_temp_var, declit->value);
}

void generate_reallit(node *reallit) {

    remove_underscores(reallit->value);
    printf("%%%d = fadd double 0.0, %.16E\n", ++cur_temp_var, atof(reallit->value));
}

void generate_boollit(node* boollit) {
    printf("%%%d = trunc i32 %d to i1\n", ++cur_temp_var, strcmp(boollit->value, "true") == 0 ? 1 : 0);
}

void generate_varDecl(node *varDecl) {
    printf("%%%s = alloca %s\n", varDecl->child[1]->value, get_llvm_type(varDecl->child[0]->name));
}

void generate_id(node *id) {
    if(id->table_el->is_global == 1) {
        printf("%%%d = load %s, %s* @"GLOBAL_PREFIX"%s\n", ++cur_temp_var, get_llvm_type(id->anotation), get_llvm_type(id->anotation), id->value);
    }
    else if(id->table_el->is_param == 1) {
        printf("%%%d = load %s, %s* %%%d", ++cur_temp_var, get_llvm_type(id->anotation), get_llvm_type(id->anotation), id->table_el->param_number);
    }
    else {
        printf("%%%d = load %s, %s* %%%s\n", ++cur_temp_var, get_llvm_type(id->anotation), get_llvm_type(id->anotation), id->value);
    }
}

void generate_assign(node *assign) {
    get_proper_gen_function(assign->child[1]->name)(assign->child[1]);
    if (strcmp(assign->child[0]->anotation, "Double") == 0 && strcmp(assign->child[1]->anotation, "Int") == 0){
        printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, cur_temp_var);
        cur_temp_var++;
    }
    if(assign->child[0]->table_el->is_global == 1) {
        printf("store %s %%%d, %s* @"GLOBAL_PREFIX"%s\n", get_llvm_type(assign->anotation), cur_temp_var, get_llvm_type(assign->child[0]->anotation), assign->child[0]->value);
    }
    else if(assign->child[0]->table_el->is_param == 0) {
        printf("store %s %%%d, %s* %%%s\n", get_llvm_type(assign->anotation), cur_temp_var, get_llvm_type(assign->child[0]->anotation), assign->child[0]->value);
    }
    else {
        printf("store %s %%%d, %s* %%%d\n", get_llvm_type(assign->anotation), cur_temp_var, get_llvm_type(assign->child[0]->anotation), assign->child[0]->table_el->param_number);
    }
}

void generate_parseArgs(node *parseArgs) {
    get_proper_gen_function(parseArgs->child[1]->name)(parseArgs->child[1]);
    printf("%%%d = add i32 %%%d, 1\n", cur_temp_var+1, cur_temp_var); //Sums 1 because java arguments dont include the prgram call
    cur_temp_var++;
    printf("%%%d = getelementptr inbounds i8*, i8** %%"ARGS_PREFIX"argv, i32 %%%d\n", cur_temp_var+1, cur_temp_var); cur_temp_var++;
    printf("%%%d = load i8*, i8** %%%d\n", cur_temp_var+1, cur_temp_var); cur_temp_var++;
    printf("%%%d =  call i32 @atoi(i8* %%%d)\n", cur_temp_var+1, cur_temp_var); cur_temp_var++;
}

void generate_length(node *length) {
    printf("%%%d = sub i32 %%"ARGS_PREFIX"argc, 1\n", ++cur_temp_var);
    printf("%%%d = add i32 0, %%%d\n", cur_temp_var+1, cur_temp_var);
    cur_temp_var++;
}

void generate_minus(node *minus) {
    get_proper_gen_function(minus->child[0]->name)(minus->child[0]);
    if(strcmp(minus->child[0]->anotation, "Int") == 0) {
        printf("%%%d = sub i32 0, %%%d\n", cur_temp_var+1, cur_temp_var); cur_temp_var++;
    }
    else {
        printf("%%%d = fsub double -0.000000e+00, %%%d\n", cur_temp_var+1, cur_temp_var); cur_temp_var++;
    }
}

void generate_plus(node *plus) {
    get_proper_gen_function(plus->child[0]->name)(plus->child[0]);
}

void generate_not(node *not) {
    get_proper_gen_function(not->child[0]->name)(not->child[0]);
    printf("%%%d = zext i1 %%%d to i32\n", cur_temp_var+1, cur_temp_var); cur_temp_var++;
    printf("%%%d = icmp eq i32 %%%d, 0\n", cur_temp_var+1, cur_temp_var); cur_temp_var++;
}

void generate_eq(node *eq) {
    get_proper_gen_function(eq->child[0]->name)(eq->child[0]);
    int arg1 = cur_temp_var;
    get_proper_gen_function(eq->child[1]->name)(eq->child[1]);
    int arg2 = cur_temp_var;

    if(strcmp(eq->child[0]->anotation, "Double") == 0 || strcmp(eq->child[1]->anotation, "Double") == 0) {
        if(strcmp(eq->child[0]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg1);
            arg1 = ++cur_temp_var;
        }
        if(strcmp(eq->child[1]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg2);
            arg2 = ++cur_temp_var;
        }
        printf("%%%d = fcmp oeq double %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
    else if(strcmp(eq->child[0]->anotation, "Int") == 0) {
        printf("%%%d = icmp eq i32 %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
    else {
        //THEY ARE BOTH BOOLEANS
        printf("%%%d = zext i1 %%%d to i32\n", cur_temp_var+1, arg1);
        cur_temp_var++;
        printf("%%%d = zext i1 %%%d to i32\n", cur_temp_var+1, arg2);
        cur_temp_var++;
        printf("%%%d = icmp eq i32 %%%d, %%%d\n", cur_temp_var+1, cur_temp_var, cur_temp_var-1);
        cur_temp_var++;
    }
}

void generate_neq(node *neq) {
    get_proper_gen_function(neq->child[0]->name)(neq->child[0]);
    int arg1 = cur_temp_var;
    get_proper_gen_function(neq->child[1]->name)(neq->child[1]);
    int arg2 = cur_temp_var;

    if(strcmp(neq->child[0]->anotation, "Double") == 0 || strcmp(neq->child[1]->anotation, "Double") == 0) {
        if(strcmp(neq->child[0]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg1);
            arg1 = ++cur_temp_var;
        }
        if(strcmp(neq->child[1]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg2);
            arg2 = ++cur_temp_var;
        }
        printf("%%%d = fcmp une double %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
    else if(strcmp(neq->child[0]->anotation, "Int") == 0) {
        printf("%%%d = icmp ne i32 %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
    else {
        //THEY ARE BOTH BOOLEANS
        printf("%%%d = zext i1 %%%d to i32\n", cur_temp_var+1, arg1);
        cur_temp_var++;
        printf("%%%d = zext i1 %%%d to i32\n", cur_temp_var+1, arg2);
        cur_temp_var++;
        printf("%%%d = icmp ne i32 %%%d, %%%d\n", cur_temp_var+1, cur_temp_var, cur_temp_var-1);
        cur_temp_var++;
    }
}

void generate_lt(node *lt) {
    get_proper_gen_function(lt->child[0]->name)(lt->child[0]);
    int arg1 = cur_temp_var;
    get_proper_gen_function(lt->child[1]->name)(lt->child[1]);
    int arg2 = cur_temp_var;

    if(strcmp(lt->child[0]->anotation, "Double") == 0 || strcmp(lt->child[1]->anotation, "Double") == 0) {
        if(strcmp(lt->child[0]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg1);
            arg1 = ++cur_temp_var;
        }
        if(strcmp(lt->child[1]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg2);
            arg2 = ++cur_temp_var;
        }
        printf("%%%d = fcmp olt double %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
    else {
        //They are both ints
        printf("%%%d = icmp slt i32 %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
}

void generate_leq(node *leq) {
    get_proper_gen_function(leq->child[0]->name)(leq->child[0]);
    int arg1 = cur_temp_var;
    get_proper_gen_function(leq->child[1]->name)(leq->child[1]);
    int arg2 = cur_temp_var;

    if(strcmp(leq->child[0]->anotation, "Double") == 0 || strcmp(leq->child[1]->anotation, "Double") == 0) {
        if(strcmp(leq->child[0]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg1);
            arg1 = ++cur_temp_var;
        }
        if(strcmp(leq->child[1]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg2);
            arg2 = ++cur_temp_var;
        }
        printf("%%%d = fcmp ole double %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
    else {
        //They are both ints
        printf("%%%d = icmp sle i32 %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
}

void generate_gt(node *gt) {
    get_proper_gen_function(gt->child[0]->name)(gt->child[0]);
    int arg1 = cur_temp_var;
    get_proper_gen_function(gt->child[1]->name)(gt->child[1]);
    int arg2 = cur_temp_var;

    if(strcmp(gt->child[0]->anotation, "Double") == 0 || strcmp(gt->child[1]->anotation, "Double") == 0) {
        if(strcmp(gt->child[0]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg1);
            arg1 = ++cur_temp_var;
        }
        if(strcmp(gt->child[1]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg2);
            arg2 = ++cur_temp_var;
        }
        printf("%%%d = fcmp ogt double %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
    else {
        //They are both ints
        printf("%%%d = icmp sgt i32 %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
}

void generate_geq(node *geq) {
    get_proper_gen_function(geq->child[0]->name)(geq->child[0]);
    int arg1 = cur_temp_var;
    get_proper_gen_function(geq->child[1]->name)(geq->child[1]);
    int arg2 = cur_temp_var;

    if(strcmp(geq->child[0]->anotation, "Double") == 0 || strcmp(geq->child[1]->anotation, "Double") == 0) {
        if(strcmp(geq->child[0]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg1);
            arg1 = ++cur_temp_var;
        }
        if(strcmp(geq->child[1]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg2);
            arg2 = ++cur_temp_var;
        }
        printf("%%%d = fcmp oge double %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
    else {
        //They are both ints
        printf("%%%d = icmp sge i32 %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
}

void generate_add(node *add) {
    get_proper_gen_function(add->child[0]->name)(add->child[0]);
    int arg1 = cur_temp_var;
    get_proper_gen_function(add->child[1]->name)(add->child[1]);
    int arg2 = cur_temp_var;

    if(strcmp(add->child[0]->anotation, "Double") == 0 || strcmp(add->child[1]->anotation, "Double") == 0) {
        if(strcmp(add->child[0]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg1);
            arg1 = ++cur_temp_var;
        }
        if(strcmp(add->child[1]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg2);
            arg2 = ++cur_temp_var;
        }
        printf("%%%d = fadd double %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
    else {
        //They are both ints
        printf("%%%d = add i32 %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
}

void generate_sub(node *sub) {
    get_proper_gen_function(sub->child[0]->name)(sub->child[0]);
    int arg1 = cur_temp_var;
    get_proper_gen_function(sub->child[1]->name)(sub->child[1]);
    int arg2 = cur_temp_var;

    if(strcmp(sub->child[0]->anotation, "Double") == 0 || strcmp(sub->child[1]->anotation, "Double") == 0) {
        if(strcmp(sub->child[0]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg1);
            arg1 = ++cur_temp_var;
        }
        if(strcmp(sub->child[1]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg2);
            arg2 = ++cur_temp_var;
        }
        printf("%%%d = fsub double %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
    else {
        //They are both ints
        printf("%%%d = sub i32 %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
}

void generate_mul(node *mul) {
    get_proper_gen_function(mul->child[0]->name)(mul->child[0]);
    int arg1 = cur_temp_var;
    get_proper_gen_function(mul->child[1]->name)(mul->child[1]);
    int arg2 = cur_temp_var;

    if(strcmp(mul->child[0]->anotation, "Double") == 0 || strcmp(mul->child[1]->anotation, "Double") == 0) {
        if(strcmp(mul->child[0]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg1);
            arg1 = ++cur_temp_var;
        }
        if(strcmp(mul->child[1]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg2);
            arg2 = ++cur_temp_var;
        }
        printf("%%%d = fmul double %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
    else {
        //They are both ints
        printf("%%%d = mul i32 %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
}

void generate_div(node *divi) {
    get_proper_gen_function(divi->child[0]->name)(divi->child[0]);
    int arg1 = cur_temp_var;
    get_proper_gen_function(divi->child[1]->name)(divi->child[1]);
    int arg2 = cur_temp_var;

    if(strcmp(divi->child[0]->anotation, "Double") == 0 || strcmp(divi->child[1]->anotation, "Double") == 0) {
        if(strcmp(divi->child[0]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg1);
            arg1 = ++cur_temp_var;
        }
        if(strcmp(divi->child[1]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg2);
            arg2 = ++cur_temp_var;
        }
        printf("%%%d = fdiv double %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
    else {
        //They are both ints
        printf("%%%d = sdiv i32 %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
}

void generate_mod(node *mod) {
    get_proper_gen_function(mod->child[0]->name)(mod->child[0]);
    int arg1 = cur_temp_var;
    get_proper_gen_function(mod->child[1]->name)(mod->child[1]);
    int arg2 = cur_temp_var;

    if(strcmp(mod->child[0]->anotation, "Double") == 0 || strcmp(mod->child[1]->anotation, "Double") == 0) {
        if(strcmp(mod->child[0]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg1);
            arg1 = ++cur_temp_var;
        }
        if(strcmp(mod->child[1]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, arg2);
            arg2 = ++cur_temp_var;
        }
        printf("%%%d = frem double %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
    else {
        //They are both ints
        printf("%%%d = srem i32 %%%d, %%%d\n", cur_temp_var+1, arg1, arg2);
        cur_temp_var++;
    }
}

void generate_block(node* block) {
    int i;
	for (i = 0; i < block->n_child; i++){
        if (block->child[i] == NULL)
            continue;
        else
             get_proper_gen_function(block->child[i]->name)(block->child[i]);
    }
}

void generate_or(node *or) {

    int my_labels = last_reserved_label+1;
    last_reserved_label += 2;

    get_proper_gen_function(or->child[0]->name)(or->child[0]);

    int last_label_1 = last_used_label;

    printf("br i1 %%%d, label %%"LABEL_PREFIX"%d, label %%"LABEL_PREFIX"%d\n", cur_temp_var, my_labels+1, my_labels);

    printf("\n"LABEL_PREFIX"%d:\n", my_labels); last_used_label = my_labels;
    get_proper_gen_function(or->child[1]->name)(or->child[1]);

    int last_label_2 = last_used_label;
    printf("br label %%"LABEL_PREFIX"%d\n", my_labels+1);

    printf("\n"LABEL_PREFIX"%d:\n", my_labels+1); last_used_label = my_labels+1;
    printf("%%%d = phi i1 [ true, %%"LABEL_PREFIX"%d ], [ %%%d, %%"LABEL_PREFIX"%d ]\n", cur_temp_var+1, last_label_1, cur_temp_var, last_label_2);
    cur_temp_var++;
}

void generate_and(node *and) {

    int my_labels = last_reserved_label+1;
    last_reserved_label += 2;

    get_proper_gen_function(and->child[0]->name)(and->child[0]);

    int last_label_1 = last_used_label;

    printf("br i1 %%%d, label %%"LABEL_PREFIX"%d, label %%"LABEL_PREFIX"%d\n", cur_temp_var, my_labels, my_labels+1);

    printf("\n"LABEL_PREFIX"%d:\n", my_labels); last_used_label = my_labels;
    get_proper_gen_function(and->child[1]->name)(and->child[1]);

    int last_label_2 = last_used_label;
    printf("br label %%"LABEL_PREFIX"%d\n", my_labels+1);

    printf("\n"LABEL_PREFIX"%d:\n", my_labels+1); last_used_label = my_labels+1;
    printf("%%%d = phi i1 [ false, %%"LABEL_PREFIX"%d ], [ %%%d, %%"LABEL_PREFIX"%d ]\n", cur_temp_var+1, last_label_1, cur_temp_var, last_label_2);
    cur_temp_var++;
}

void generate_doWhile(node *doWhile) {

    int my_labels = last_reserved_label + 1;
    last_reserved_label += 2; //I Have two labels to use

    printf("br label %%"LABEL_PREFIX"%d\n", my_labels);
    printf("\n"LABEL_PREFIX"%d:\n", my_labels); last_used_label = my_labels;
    get_proper_gen_function(doWhile->child[0]->name)(doWhile->child[0]);

    get_proper_gen_function(doWhile->child[1]->name)(doWhile->child[1]);
    printf("br i1 %%%d, label %%"LABEL_PREFIX"%d, label %%"LABEL_PREFIX"%d\n", cur_temp_var, my_labels, my_labels+1);
    printf("\n"LABEL_PREFIX"%d:\n", my_labels+1); last_used_label = my_labels+1;

}

void generate_while(node *while_n) {

    int my_labels = last_reserved_label + 1;
    last_reserved_label += 3; //I Have three labels to use


    printf("br label %%"LABEL_PREFIX"%d\n", my_labels); last_used_label = my_labels;
    printf("\n"LABEL_PREFIX"%d:\n", my_labels); last_used_label = my_labels;
    get_proper_gen_function(while_n->child[0]->name)(while_n->child[0]);
    printf("br i1 %%%d, label %%"LABEL_PREFIX"%d, label %%"LABEL_PREFIX"%d\n", cur_temp_var, my_labels+1, my_labels+2);

    printf("\n"LABEL_PREFIX"%d:\n", my_labels+1); last_used_label = my_labels+1;
    get_proper_gen_function(while_n->child[1]->name)(while_n->child[1]);
    printf("br label %%"LABEL_PREFIX"%d\n", my_labels); last_used_label = my_labels;

    printf("\n"LABEL_PREFIX"%d:\n", my_labels+2); last_used_label = my_labels+2;

}

void generate_if(node *if_n) {

    int my_labels = last_reserved_label + 1;
    last_reserved_label += 3; //I Have two labels to use

    get_proper_gen_function(if_n->child[0]->name)(if_n->child[0]);
    printf("br i1 %%%d, label %%"LABEL_PREFIX"%d, label %%"LABEL_PREFIX"%d\n", cur_temp_var, my_labels, my_labels+1);

    printf("\n"LABEL_PREFIX"%d:\n", my_labels); last_used_label = my_labels;
    get_proper_gen_function(if_n->child[1]->name)(if_n->child[1]);
    printf("br label %%"LABEL_PREFIX"%d\n", my_labels+2); last_used_label = my_labels+2;


    printf("\n"LABEL_PREFIX"%d:\n", my_labels+1); last_used_label = my_labels+1;
    get_proper_gen_function(if_n->child[2]->name)(if_n->child[2]);
    printf("br label %%"LABEL_PREFIX"%d\n", my_labels+2); last_used_label = my_labels+2;


    printf("\n"LABEL_PREFIX"%d:\n", my_labels+2); last_used_label = my_labels+2;

}

void generate_call(node *call) {
    int n_args = call->child[0]->method_anotation->n_args;

    int i, *arg_temps;

    if(n_args == 1 && strcmp(call->child[1]->anotation, "StringArray") == 0) {
        if(strcmp(call->child[0]->method_anotation->return_type, "Void") != 0) {
            printf("%%%d = ", cur_temp_var+1);
            cur_temp_var++;
        }
        printf("call %s @"FUNCTION_PREFIX"%s", get_llvm_type(call->child[0]->method_anotation->return_type), call->child[0]->method_anotation->name);
        for(i = 0; i < call->child[0]->method_anotation->n_args; i++) {
            printf(".%s", call->child[0]->method_anotation->arg_types[i]);
        }
        printf("(");
        printf("i32 %%"ARGS_PREFIX"argc, i8** %%"ARGS_PREFIX"argv)\n");
        return;
    }

    arg_temps = malloc(n_args*sizeof(int));

    int aux = 0;
    for(i = 1; i < call->n_child; i++) {
        if(call->child[i] == NULL) {
            continue;
        }
        get_proper_gen_function(call->child[i]->name)(call->child[i]);
        if(strcmp(call->child[i]->anotation, "Int") == 0 && strcmp(call->child[0]->method_anotation->arg_types[aux], "Double") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, cur_temp_var);
            cur_temp_var++;
        }
        arg_temps[aux++] = cur_temp_var;
    }
    if(strcmp(call->child[0]->method_anotation->return_type, "Void") != 0) {
        printf("%%%d = ", cur_temp_var+1);
        cur_temp_var++;
    }
    printf("call %s @"FUNCTION_PREFIX"%s", get_llvm_type(call->child[0]->method_anotation->return_type), call->child[0]->method_anotation->name);
    for(i = 0; i < call->child[0]->method_anotation->n_args; i++) {
        printf(".%s", call->child[0]->method_anotation->arg_types[i]);
    }
    printf("(");
    for(i = 0; i < call->child[0]->method_anotation->n_args; i++) {
        if(i != 0) {
            printf(", ");
        }
        printf("%s %%%d", get_llvm_type(call->child[0]->method_anotation->arg_types[i]), arg_temps[i]);
    }
    printf(")\n");

    free(arg_temps);

}

void generate_return(node *return_n) {
    if(return_n->n_child == 1) {
        get_proper_gen_function(return_n->child[0]->name)(return_n->child[0]);
        if(strcmp(return_n->return_anotation, "Double") == 0 && strcmp(return_n->child[0]->anotation, "Int") == 0) {
            printf("%%%d = sitofp i32 %%%d to double\n", cur_temp_var+1, cur_temp_var);
            cur_temp_var++;
        }
        printf("ret %s %%%d\n", get_llvm_type(return_n->return_anotation), cur_temp_var);
        cur_temp_var++;
    }
    else {
        printf("ret void\n");
    }

}

void do_nothing(node *node) {

}

function_gen_type get_proper_gen_function(char *node_name) {
    if(strcmp(node_name, "Print") == 0) {
        return generate_print;
    }
    else if(strcmp(node_name, "DecLit") == 0) {
        return generate_declit;
    }
    else if(strcmp(node_name, "RealLit") == 0) {
        return generate_reallit;
    }
    else if(strcmp(node_name, "BoolLit") == 0) {
        return generate_boollit;
    }
    else if(strcmp(node_name, "VarDecl") == 0) {
        return generate_varDecl;
    }
    else if(strcmp(node_name, "Id") == 0) {
        return generate_id;
    }
    else if(strcmp(node_name, "Assign") == 0) {
        return generate_assign;
    }
    else if (strcmp(node_name, "Length") == 0){
        return generate_length;
    }
    else if(strcmp(node_name, "ParseArgs") == 0) {
        return generate_parseArgs;
    }
    else if(strcmp(node_name, "Minus") == 0) {
        return generate_minus;
    }
    else if(strcmp(node_name, "Or") == 0) {
        return generate_or;
    }
    else if(strcmp(node_name, "And") == 0) {
        return generate_and;
    }
    else if(strcmp(node_name, "Eq") == 0) {
        return generate_eq;
    }
    else if(strcmp(node_name, "Neq") == 0) {
        return generate_neq;
    }
    else if (strcmp(node_name, "Lt") == 0){
        return generate_lt;
    }
    else if(strcmp(node_name, "Leq") == 0){
        return generate_leq;
    }
    else if(strcmp(node_name, "Gt") == 0){
        return generate_gt;
    }
    else if(strcmp(node_name, "Geq") == 0){
        return generate_geq;
    }
    else if(strcmp(node_name, "Plus") == 0) {
        return generate_plus;
    }
    else if(strcmp(node_name, "Add") == 0) {
        return generate_add;
    }
    else if(strcmp(node_name, "Sub") == 0) {
        return generate_sub;
    }
    else if(strcmp(node_name, "Mul") == 0) {
        return generate_mul;
    }
    else if(strcmp(node_name, "Div") == 0) {
        return generate_div;
    }
    else if(strcmp(node_name, "Mod") == 0) {
        return generate_mod;
    }
    else if(strcmp(node_name, "Not") == 0) {
        return generate_not;
    }
    else if(strcmp(node_name, "Block") == 0) {
        return generate_block;
    }
    else if(strcmp(node_name, "DoWhile") == 0) {
        return generate_doWhile;
    }
    else if(strcmp(node_name, "While") == 0) {
        return generate_while;
    }
    else if(strcmp(node_name, "If") == 0) {
        return generate_if;
    }
    else if(strcmp(node_name, "Call") == 0) {
        return generate_call;
    }
    else if(strcmp(node_name, "Return") == 0) {
        return generate_return;
    }
    else {
        return do_nothing;
    }
}

int print_as_llvm_string(char *str, int do_print) {
    int len_counter = 0;
    int pos = 0;

    while(str[pos] != '\0') {
        if(str[pos] == '\"') {
            pos++;
            continue;
        }
        else if(str[pos] == '%') {
            if(do_print)
                printf("%%%%");
            pos++;
            len_counter += 2;
            continue;
        }
        else if(str[pos] == '\\') {
            if(str[pos+1] == 'n') {
                if(do_print)
                    printf("\\0A");
            }
            else if(str[pos+1] == 't') {
                if(do_print)
                    printf("\\09");
            }
            else if(str[pos+1] == 'f') {
                if(do_print)
                    printf("\\0C");
            }
            else if(str[pos+1] == 'r') {
                if(do_print)
                    printf("\\0D");
            }
            else if(str[pos+1] == '"') {
                if(do_print)
                    printf("\\22");
            }
            else {
                if(do_print)
                    printf("\\5C");
            }
            len_counter += 1;
            pos += 2;
            continue;
        }
        else {
            if(do_print)
                putchar(str[pos]);
            len_counter++;
            pos++;
        }
    }
    return len_counter;
}

void search_static_strings(node* root) {
    int i;
    for(i=0; i<root->n_child; i++) {
        if(root->child[i] == NULL) {
            continue;
        }

        else if(strcmp(root->child[i]->name, "StrLit") == 0) {
            int len = print_as_llvm_string(root->child[i]->value, 0);
            printf("@"STRING_CONST_PREFIX"%d = private unnamed_addr constant [%d x i8] c\"", cur_string_lit++, len+2);
            print_as_llvm_string(root->child[i]->value,1);
            printf("\\0A");
            printf("\\00\"\n");
        }
        else {
            search_static_strings(root->child[i]);
        }
    }
}

void allocate_var(int is_global, table_element *t){
    if(is_global){
        printf("@"GLOBAL_PREFIX"%s = global %s 0", t->name, get_llvm_type(t->type));
        if(strcmp(t->type, "Double") == 0) {
            printf(".0");
        }
    }
    else {
        printf("%%"LOCAL_PREFIX"%s = alloca %s", t->name, get_llvm_type(t->type));
    }
    printf("\n");
}

void generate_class_vars(node *root, class_environment *class_env) {
    table_element *cur = class_env->vars;

    while(cur != NULL) {
        allocate_var(1, cur);
        cur = cur->next;
    }
}

char* get_llvm_type(char *ja_type) {
    if(strcmp(ja_type, "Int") == 0) {
        return "i32";
    }
    else if(strcmp(ja_type, "Double") == 0) {
        return "double";
    }
    else if(strcmp(ja_type, "Bool") == 0) {
        return "i1";
    }
    else if(strcmp(ja_type, "Void") == 0) {
        return "void";
    }
    else if(strcmp(ja_type, "String") == 0) {
        return "i8*";
    }
    else if(strcmp(ja_type, "StringArray") == 0) {
        return "i8**";
    }
    return "FAILURE IN GET_LLVM_TYPE";
}
