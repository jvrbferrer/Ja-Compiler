%{
#include "tree.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "y.tab.h"

int linha = 1;
int coluna = 1;

int linha_com;
int col_com;

int linha_str;
int col_str;

int buffer_size;
int string_size;
char *string;

extern int is_error;


int printTokens = 0;
int justLexical = 0;
int flag_bad_str = 0;

int string_to_bool(char *string);
void yyerror (const char *s);
char* make_string(char *s);

%}

%X COMENTARIO STR COMENTARIO_LINHA

numero [0-9]+
line_terminator \r\n|\n|\r

id [a-zA-Z_$][a-zA-Z_$0-9]*

declit 0|[1-9]+("_"*[0-9]+)*
digits  [0-9]+("_"*[0-9]+)*

reallit {digits}"."{digits}?{expoent}?|"."{digits}{expoent}?|{digits}{expoent}
expoent [eE][+-]?{digits}


%%
"\""                                {
                                        BEGIN STR;
                                        string = malloc(10*sizeof(char));
                                        buffer_size = 10;
                                        string[0] ='\0';
                                        string_size = 0;
                                        flag_bad_str = 0;
                                        col_str = coluna;
                                        linha_str = linha;
                                        coluna+=yyleng;
                                    }
<STR>\"                             {
                                        BEGIN 0;
                                        coluna+=yyleng;
                                        yytext = make_string(string); //NEED TO FREE yytext before this??
                                        yyleng = strlen(yytext);
                                        if (printTokens == 1 && flag_bad_str == 0)
                                            printf("STRLIT(%s)\n", yytext);
                                        (yylval.val).linha = linha;
                                        (yylval.val).coluna = coluna-yyleng;
                                        (yylval.val).string=(char*)strdup(yytext);
                                        free(string);
                                        if (justLexical == 0 && flag_bad_str == 0) return STRLIT;
                                    }
<STR>\\[fnrt\\\"]                   {
                                        char *i = yytext;
                                        string_size+=2;
                                        if (string_size >= buffer_size - 1){
                                            string = realloc(string, (buffer_size + 10)*sizeof(char));
                                            buffer_size+=10;
                                        }
                                        strcat(string, i);
                                        coluna+=yyleng;
                                    }
<STR><<EOF>>                        {
                                        is_error = 1;
                                        printf("Line %d, col %d: unterminated string literal\n", linha_str, col_str); yyterminate();
                                        free(string);
                                    }
<STR>{line_terminator}              {
                                        is_error = 1;
                                        printf("Line %d, col %d: unterminated string literal\n", linha_str, col_str);
                                        BEGIN 0;
                                        free(string);
                                        coluna = 1;
                                        linha++;
                                    }
<STR>\\[^\n\r]?                     {
                                        is_error = 1;
                                        printf("Line %d, col %d: invalid escape sequence (%s)\n", linha, coluna, yytext);
                                        coluna+=yyleng;
                                        flag_bad_str = 1;

                                    }
<STR>.                              {
                                        char *i = yytext;
                                        string_size++;

                                        if (string_size >= buffer_size - 1){
                                            string = realloc(string, (buffer_size + 10)*sizeof(char));
                                            buffer_size+=10;
                                        }
                                        strcat(string, i);
                                        coluna+=yyleng;
                                    }


"/*"                                {BEGIN COMENTARIO;linha_com = linha; col_com = coluna;coluna+=yyleng;}
<COMENTARIO>"*/"                    {BEGIN 0;coluna+=yyleng;}
<COMENTARIO>{line_terminator}       {linha++;coluna=1;}
<COMENTARIO><<EOF>>                 {is_error = 1;printf("Line %d, col %d: unterminated comment\n", linha_com, col_com);yyterminate();}
<COMENTARIO>.                       {coluna+=yyleng;};

"//"                                {BEGIN COMENTARIO_LINHA;coluna+=yyleng;}

<COMENTARIO_LINHA>{line_terminator} {BEGIN 0;linha++;coluna=1;}
<COMENTARIO_LINHA>.                 {coluna+=yyleng;}

class                       {if (printTokens == 1)printf("CLASS\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return CLASS;}
do                          {if (printTokens == 1)printf("DO\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return DO;}
"."length                   {if (printTokens == 1)printf("DOTLENGTH\n");coluna += yyleng;  (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return DOTLENGTH;}
double                      {if (printTokens == 1)printf("DOUBLE\n");coluna += yyleng;(yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return DOUBLE;}
else                        {if (printTokens == 1)printf("ELSE\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return ELSE;}
if                          {if (printTokens == 1)printf("IF\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng;(yylval.val).string = NULL;if (justLexical == 0) return IF;}
int                         {if (printTokens == 1)printf("INT\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL;if (justLexical == 0) return INT;}
Integer\.parseInt           {if (printTokens == 1)printf("PARSEINT\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return PARSEINT;}
System\.out\.println        {if (printTokens == 1)printf("PRINT\n");coluna += yyleng;(yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return PRINT;}
boolean                     {if (printTokens == 1)printf("BOOL\n");coluna += yyleng;(yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return BOOL;}
public                      {if (printTokens == 1)printf("PUBLIC\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return PUBLIC;}
return                      {if (printTokens == 1)printf("RETURN\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return RETURN;}
static                      {if (printTokens == 1)printf("STATIC\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return STATIC;}
String                      {if (printTokens == 1)printf("STRING\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return STRING;}
void                        {if (printTokens == 1)printf("VOID\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return VOID;}
while                       {if (printTokens == 1)printf("WHILE\n");coluna += yyleng;(yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return WHILE;}

"("                         {if (printTokens == 1)printf("OCURV\n");coluna += yyleng;(yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return OCURV;}
")"                         {if (printTokens == 1)printf("CCURV\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return CCURV;}
"{"                         {if (printTokens == 1)printf("OBRACE\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return OBRACE;}
"}"                         {if (printTokens == 1)printf("CBRACE\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return CBRACE;}
","                         {if (printTokens == 1)printf("COMMA\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return COMMA;}
";"                         {if (printTokens == 1)printf("SEMI\n");coluna += yyleng;(yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return SEMI;}
"="                         {if (printTokens == 1)printf("ASSIGN\n");coluna += yyleng;(yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return ASSIGN;}
"!"                         {if (printTokens == 1)printf("NOT\n");coluna += yyleng;(yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return NOT;}
"%"                         {if (printTokens == 1)printf("MOD\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL;if (justLexical == 0) return MOD;}
"/"                         {if (printTokens == 1)printf("DIV\n");coluna += yyleng;(yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return DIV;}
"*"                         {if (printTokens == 1)printf("STAR\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return STAR;}
"-"                         {if (printTokens == 1)printf("MINUS\n");coluna += yyleng;(yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return MINUS;}
"+"                         {if (printTokens == 1)printf("PLUS\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return PLUS;}
">="                        {if (printTokens == 1)printf("GEQ\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return GEQ;}
"<="                        {if (printTokens == 1)printf("LEQ\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return LEQ;}
"!="                        {if (printTokens == 1)printf("NEQ\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return NEQ;}
"=="                        {if (printTokens == 1)printf("EQ\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return EQ;}
"<"                         {if (printTokens == 1)printf("LT\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return LT;}
">"                         {if (printTokens == 1)printf("GT\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return GT;}
"||"                        {if (printTokens == 1)printf("OR\n");coluna += yyleng;(yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return OR;}
"&&"                        {if (printTokens == 1)printf("AND\n");coluna += yyleng;(yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return AND;}
"["                         {if (printTokens == 1)printf("OSQUARE\n");coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return OSQUARE;}
"]"                         {if (printTokens == 1)printf("CSQUARE\n");coluna += yyleng;(yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = NULL; if (justLexical == 0) return CSQUARE;}

abstract|assert|break|byte|case|catch|char|const|continue|default|enum|extends|final|finally|float|for|goto|implements|import|instanceof|interface|long|native|new|package|private|throws|transient|short|try|strictfp|volatile|super|switch|synchronized|this|throw|"++"|"--"|null|Integer|System|protected {if (printTokens == 1)printf("RESERVED(%s)\n", yytext);coluna += yyleng; (yylval.val).string=(char*)strdup(yytext); (yylval.val).linha = linha; (yylval.val).coluna = coluna-yyleng;if (justLexical == 0) return RESERVED;}

(true)|(false)              {if (printTokens == 1)printf("BOOLLIT(%s)\n", yytext);coluna += yyleng;(yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng; (yylval.val).string = (char*)strdup(yytext); if (justLexical == 0) return BOOLLIT;}
{reallit}                   {if (printTokens == 1)printf("REALLIT(%s)\n", yytext);coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng;(yylval.val).string = (char*)strdup(yytext); if (justLexical == 0) return REALLIT;}
{declit}                    {if (printTokens == 1)printf("DECLIT(%s)\n", yytext);coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng;(yylval.val).string = (char*)strdup(yytext); if (justLexical == 0) return DECLIT;}
{id}                        {if (printTokens == 1)printf("ID(%s)\n", yytext);coluna += yyleng; (yylval.val).linha = linha;(yylval.val).coluna = coluna-yyleng;(yylval.val).string = (char*)strdup(yytext); if (justLexical == 0) return ID;}


{line_terminator}   {linha++;coluna=1;}
" "|\t|\f           {coluna+=yyleng;}
.                   {is_error = 1;printf("Line %d, col %d: illegal character (%s)\n", linha, coluna, yytext);coluna += yyleng;}

%%
int yywrap() {
    return 1;
}

void yyerror (const char *s) {
    if(strcmp(yytext, "")==0) {
        yyleng=0;
    }
    is_error = 1;
    printf ("Line %d, col %ld: %s: %s\n", linha, coluna - yyleng, s, yytext);
}

int string_to_bool(char *string){
    if(strcmp(string, "true") == 0){
        return 1;
    }

    else{
        return 0;
    }
}

char* make_string(char *s){
    char *d = malloc(strlen(s)+3); //plus 2" plus \0
    if(d==NULL){
        return NULL;
    }
    d[0]='"';
    d[1]='\0';
    strcat(d,s);
    strcat(d,"\"");
    return d;
}
