%{
    #include "tree.h"
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    int yylex(void);
    void yyerror (const char *s);
    extern int printTokens;
    extern int justLexical;
    int tree_flag = 0;
    int table_flag = 0;
    int yylex_destroy(void);
    node* root;
    int is_error = 0;

    extern int yyleng;

    extern int linha;
    extern int coluna;
%}

%union{
    struct token_val_ val;
    struct Node* node_ptr;
}

%token <val> STRLIT;
%token <val> RESERVED;
%token <val> ID
%token <val> BOOLLIT
%token <val> REALLIT
%token <val> DECLIT
%token <val> ASSIGN SEMI COMMA;
%token <val> WHILE DO;
%token <val> PUBLIC STATIC CLASS;
%token <val> PRINT DOTLENGTH PARSEINT RETURN;
%token <val> INT BOOL DOUBLE STRING VOID;
%token <val> ELSE IF;
%token <val> PLUS MINUS STAR DIV MOD;
%token <val> GEQ LEQ NEQ EQ LT GT;
%token <val> OR AND NOT;
%token <val> OSQUARE CSQUARE;
%token <val> OBRACE CBRACE;
%token <val> OCURV CCURV;
%token PRECEDENCE PLUSPREC NO_ELSE

%type <node_ptr> expression assignment methodInvocation parseArgs statement params paramsRec id methodBody void varDeclRec strlit error fieldDeclRec
%type <node_ptr> program programRec fieldDecl methodDecl methodHeader typeRule methodBodyRec formalParams varDecl formalParamsRec statementRec expressionAux

%left COMMA
%left ASSIGN
%left OR
%left AND
%left EQ NEQ
%left LEQ LT GT GEQ
%left PLUS MINUS
%left STAR DIV MOD
%right PRECEDENCE
%left OBRACE OCURV OSQUARE
%nonassoc NO_ELSE
%nonassoc ELSE

%%
program: CLASS id OBRACE programRec CBRACE                  {$$ = new_node("Program", NULL, $2->linha, $2->coluna); add_child($$, $2); program_shallow_tree($$, $4);root=$$;}

programRec: fieldDecl programRec                            {$$ = new_node("ProgramRec", NULL, $1->linha, $1->coluna); add_child($$, $1); add_child($$, $2);}
    | methodDecl programRec                                 {$$ = new_node("ProgramRec", NULL, $1->linha, $1->coluna); add_child($$, $1); add_child($$, $2);}
    | SEMI programRec                                       {$$ = new_node("ProgramRec", NULL, 0, 0); add_child($$, $2);}
    | %empty                                                {$$ = NULL;}

fieldDecl: PUBLIC STATIC typeRule id fieldDeclRec SEMI      {$$ = new_node("FieldDecl", NULL, $4->linha, $4->coluna);add_child($$, $3); add_child($$, $4); add_child($$, $5);}
    | error SEMI                                            {$$ = new_node("Error", NULL,$2.linha,$2.coluna);tree_flag=0;}

fieldDeclRec: COMMA id fieldDeclRec                         {$$ = new_node("FieldDeclRec", NULL, $2->linha, $2->coluna); add_child($$, $2); add_child($$, $3);}
    | %empty                                                {$$ = NULL;}

methodDecl: PUBLIC STATIC methodHeader methodBody           {$$ = new_node("MethodDecl", NULL, $1.linha, $1.coluna); add_child($$, $3); add_child($$, $4);}

methodHeader: typeRule id OCURV formalParams CCURV          {$$ = new_node("MethodHeader", NULL, $1->linha, $1->coluna); add_child($$, $1); add_child($$, $2); add_child($$, $4);}
    | void id OCURV formalParams CCURV                      {$$ = new_node("MethodHeader", NULL, $1->linha, $1->coluna); add_child($$, $1); add_child($$, $2); add_child($$, $4);}

methodBody: OBRACE methodBodyRec CBRACE                     {$$ = new_node("MethodBody", NULL, $1.linha, $1.coluna); method_body_shallow_tree($$, $2);}

methodBodyRec: varDecl methodBodyRec                        {$$ = new_node("MethodBodyRec", NULL, $1->linha, $1->coluna); add_child($$, $1); add_child($$, $2);}
    | statement methodBodyRec                               {$$ = new_node("MethodBodyRec", NULL, 0, 0); add_child($$, $1); add_child($$, $2);}
    | %empty                                                {$$ = NULL;}

formalParams: %empty                                        {$$ = new_node("MethodParams", NULL, linha, coluna-yyleng);}
    | STRING OSQUARE CSQUARE id                             {
                                                                $$ = new_node("MethodParams", NULL,$1.linha,$1.coluna); node* params_decl = new_node("ParamDecl", NULL,$1.linha,$1.coluna);
                                                                node* string_array = new_node("StringArray", NULL,$1.linha,$1.coluna); add_child(params_decl,string_array);
                                                                add_child(params_decl,$4); add_child($$, params_decl);
                                                            }
    | typeRule id formalParamsRec                           {
                                                                $$ = new_node("MethodParams", NULL,$1->linha,$1->coluna); node* params_decl = new_node("ParamDecl", NULL,$1->linha,$1->coluna);
                                                                add_child(params_decl,$1); add_child(params_decl,$2); add_child($$, params_decl);
                                                                method_params_shallow_tree($$, $3);
                                                            }

formalParamsRec: %empty                                     {$$ = NULL;}
    | COMMA typeRule id formalParamsRec                     {$$ = new_node("FormalParamsRec",NULL, $1.linha,$1.coluna); add_child($$, $2); add_child($$, $3); add_child($$, $4);}

varDecl: typeRule id varDeclRec SEMI                        {$$ = new_node("VarDecl", NULL, $1->linha,$1->coluna); add_child($$, $1); add_child($$, $2); add_child($$, $3);}

varDeclRec: %empty                                          {$$ = NULL;}
    | COMMA id varDeclRec                                   {$$ = new_node("VarDeclRec", NULL,$2->linha,$2->coluna); add_child($$, $2); add_child($$, $3);}

typeRule: BOOL                                              {$$ = new_node("Bool", NULL, $1.linha,$1.coluna);}
    | INT                                                   {$$ = new_node("Int", NULL, $1.linha,$1.coluna);}
    | DOUBLE                                                {$$ = new_node("Double", NULL, $1.linha,$1.coluna);}

statement: RETURN SEMI                                      {$$ = new_node("Return", NULL, $1.linha,$1.coluna);}
    | RETURN expressionAux SEMI                             {$$ = new_node("Return", NULL, $1.linha,$1.coluna); add_child($$,$2);}
    | SEMI                                                  {$$ = NULL;}
    | assignment SEMI                                       {$$ = $1;}
    | methodInvocation SEMI                                 {$$ = $1;}
    | parseArgs SEMI                                        {$$ = $1;}
    | PRINT OCURV expressionAux CCURV SEMI                  {$$ = new_node("Print", NULL, $1.linha,$1.coluna); add_child($$, $3);}
    | PRINT OCURV strlit CCURV SEMI                         {$$ = new_node("Print", NULL, $1.linha,$1.coluna); add_child($$, $3);}
    | DO statement WHILE OCURV expressionAux CCURV SEMI     {$$ = new_node("DoWhile", NULL, $1.linha,$1.coluna); if($2 == NULL)add_child($$, new_node("Block", NULL,$5->linha,$5->coluna));else add_child($$, $2); add_child($$, $5);}
    | WHILE OCURV expressionAux CCURV statement             {$$ = new_node("While", NULL,$1.linha,$1.coluna); add_child($$, $3); if($5 == NULL)add_child($$, new_node("Block", NULL,$1.linha,$1.coluna));else add_child($$, $5);}
    | IF OCURV expressionAux CCURV statement ELSE statement    {
                                                                $$ = new_node("If", NULL,$1.linha,$1.coluna); add_child($$, $3); if($5==NULL) {
                                                                    add_child($$, new_node("Block", NULL, 0, 0));
                                                                }
                                                                else
                                                                    add_child($$, $5);
                                                                if($7 == NULL)
                                                                    add_child($$, new_node("Block", NULL, 0, 0));
                                                                else
                                                                    add_child($$, $7);
                                                            }
    | IF OCURV expressionAux CCURV statement %prec NO_ELSE     {
                                                                $$ = new_node("If", NULL,$1.linha,$1.coluna);
                                                                add_child($$, $3);
                                                                if($5==NULL) {
                                                                    add_child($$, new_node("Block", NULL,$3->linha,$3->coluna));
                                                                }
                                                                else {
                                                                    add_child($$, $5);
                                                                }
                                                                add_child($$, new_node("Block", NULL, 0, 0));
                                                            }
    | OBRACE statementRec CBRACE                            {$$ = new_node("Block", NULL,$1.linha,$1.coluna); statement_shallow_tree($$, $2); if($$->n_child == 1) {$$ = $$->child[0];} else if($$->child == 0){$$=NULL;}}
    | error SEMI                                            {$$ = new_node("Error", NULL, 0, 0);tree_flag=0;}

strlit: STRLIT                                              {$$ = new_node("StrLit",$1.string,$1.linha,$1.coluna);}

statementRec: %empty                                        {$$ = NULL;}
    | statement statementRec                                {$$ = new_node("StatementRec", NULL, 0, 0);add_child($$, $1);add_child($$,$2);}


expressionAux: assignment
    | expression

expression: BOOLLIT                                         {$$ = new_node("BoolLit", $1.string,$1.linha,$1.coluna);}
    | DECLIT                                                {$$ = new_node("DecLit", $1.string,$1.linha,$1.coluna);}
    | REALLIT                                               {$$ = new_node("RealLit", $1.string,$1.linha,$1.coluna);}
    | OCURV expressionAux CCURV                             {$$ = $2;}
    | PLUS expression   %prec PRECEDENCE                    {$$ = new_node("Plus",NULL,$1.linha,$1.coluna); add_child($$, $2);}
    | MINUS expression  %prec PRECEDENCE                    {$$ = new_node("Minus",NULL,$1.linha,$1.coluna); add_child($$, $2);}
    | NOT expression    %prec PRECEDENCE                    {$$ = new_node("Not",NULL,$1.linha,$1.coluna); add_child($$, $2);}
    | expression PLUS expression                            {$$ = new_node("Add",NULL,$2.linha,$2.coluna); add_child($$, $1);add_child($$, $3);}
    | expression MINUS expression                           {$$ = new_node("Sub",NULL,$2.linha,$2.coluna); add_child($$, $1);add_child($$, $3);}
    | expression STAR expression                            {$$ = new_node("Mul",NULL,$2.linha,$2.coluna); add_child($$, $1);add_child($$, $3);}
    | expression DIV expression                             {$$ = new_node("Div",NULL,$2.linha,$2.coluna); add_child($$, $1);add_child($$, $3);}
    | expression MOD expression                             {$$ = new_node("Mod",NULL,$2.linha,$2.coluna); add_child($$, $1);add_child($$, $3);}
    | expression EQ expression                              {$$ = new_node("Eq",NULL,$2.linha,$2.coluna); add_child($$, $1);add_child($$, $3);}
    | expression GEQ expression                             {$$ = new_node("Geq",NULL,$2.linha,$2.coluna); add_child($$, $1);add_child($$, $3);}
    | expression GT expression                              {$$ = new_node("Gt",NULL,$2.linha,$2.coluna); add_child($$, $1);add_child($$, $3);}
    | expression LEQ expression                             {$$ = new_node("Leq",NULL,$2.linha,$2.coluna); add_child($$, $1);add_child($$, $3);}
    | expression LT expression                              {$$ = new_node("Lt",NULL,$2.linha,$2.coluna); add_child($$, $1);add_child($$, $3);}
    | expression NEQ expression                             {$$ = new_node("Neq",NULL,$2.linha,$2.coluna); add_child($$, $1);add_child($$, $3);}
    | expression AND expression                             {$$ = new_node("And",NULL,$2.linha,$2.coluna); add_child($$, $1);add_child($$, $3);}
    | expression OR expression                              {$$ = new_node("Or",NULL,$2.linha,$2.coluna); add_child($$, $1);add_child($$, $3);}
    | id                                                    {$$ = $1;}
    | id DOTLENGTH                                          {$$ = new_node("Length",NULL,$1->linha,$1->coluna); add_child($$,$1);}
    | methodInvocation                                      {$$ = $1;}
    | parseArgs                                             {$$ = $1;}
    | OCURV error CCURV                                     {$$ = new_node("Error", NULL, 0, 0);tree_flag=0;}

parseArgs: PARSEINT OCURV id OSQUARE expressionAux CSQUARE CCURV   {$$ = new_node("ParseArgs",NULL,$1.linha,$1.coluna); add_child($$,$3); add_child($$,$5);}
    | PARSEINT OCURV error CCURV                                {$$ = new_node("Error", NULL, 0, 0);tree_flag=0;}

assignment: id ASSIGN expressionAux                   {$$ = new_node("Assign",NULL,$2.linha,$2.coluna); add_child($$,$1); add_child($$,$3);}

id: ID                                             {$$ = new_node("Id",$1.string,$1.linha,$1.coluna);}

void: VOID                                         {$$ = new_node("Void",NULL,$1.linha,$1.coluna);}

methodInvocation: id OCURV params CCURV            {$$ = new_node("Call", NULL,$1->linha,$1->coluna); add_child($$, $1); call_shallow_tree($$, $3);}
    | id OCURV error CCURV                         {$$ = new_node("Error", NULL,$1->linha,$1->coluna);tree_flag=0;}

params: %empty                                     {$$ = NULL;}
    | expressionAux paramsRec                         {$$ = new_node("ParamsRec", NULL,$1->linha,$1->coluna); add_child($$,$1); add_child($$,$2);}

paramsRec: %empty                                  {$$ = NULL;}
    | COMMA expressionAux paramsRec                   {$$ = new_node("ParamsRec", NULL,$2->linha,$2->coluna); add_child($$, $2); add_child($$, $3);}

%%

int main(int  argc, char *argv[]) {
    class_environment *class_env = NULL;

    if(argc == 2) {
        if(!strcmp(argv[1], "-l")){
            printTokens = 1;
            justLexical = 1;
            table_flag = 0;
            yylex();
        }
        else if(!strcmp(argv[1], "-1")) {
            printTokens = 0;
            justLexical = 1;
            table_flag = 0;
            yylex();
        }
        else if(!strcmp(argv[1], "-t") ) {
            printTokens = 0;
            justLexical = 0;
            tree_flag = 1;
            table_flag = 0;
            yyparse();
        }
        else if(!strcmp(argv[1], "-2")) {
            printTokens = 0;
            justLexical = 0;
            tree_flag = 0;
            table_flag = 0;
            yyparse();
        }
        else if(!strcmp(argv[1], "-s")){
            printTokens = 0;
            justLexical = 0;
            tree_flag = 1;
            table_flag = 1;
            yyparse();
            if(is_error == 0) {
                class_env = check_semantics(root, class_env);
            }
        }
        else if(!strcmp(argv[1], "-3")) {
            printTokens = 0;
            justLexical = 0;
            tree_flag = 0;
            table_flag = 0;
            yyparse();
            if(is_error == 0) {
                class_env = check_semantics(root, class_env);

            }
        }
    }
    else {
        yyparse();
        if(is_error == 0) {
            class_env = check_semantics(root, class_env);
        }
        if(is_error == 0) {
            generate_code(root, class_env);
        }
    }

    yylex_destroy();

    if(table_flag == 1) {
        print_env(class_env);
    }
    if(tree_flag==1) {
        print_tree(root,1);
    }

    free_class_env(class_env);
    free_tree(root);

    return 0;
}
