#include "tree.h"

extern int is_error;


void insert_dec_order(int is_method, class_environment *class) {
    //Method that inserts the order of the class vars and methods.
    //Used to print the var tables in orderpublic static void abb(double a, double b) {}

    decl_order *new_decl_order = malloc(sizeof(decl_order));
    new_decl_order->is_method = is_method;
    new_decl_order->next = NULL;

    decl_order *cur = class->order;
    if(cur == NULL) {
        class->order = new_decl_order;
        return;
    }
    while(cur->next != NULL) {
        cur = cur->next;
    }

    cur->next = new_decl_order;
}

table_element* new_table_element(char *name, char* type, int is_param, int is_global, int param_number) {

    table_element *new_var = malloc(sizeof(table_element));

    if(new_var == NULL){
        printf("Error allocating memory\n");
    }
    new_var->name = strdup(name);
    new_var->type = type;
    new_var->next = NULL;
    new_var->is_param = is_param;
    new_var->is_global = is_global;
    new_var->param_number = param_number;

    return new_var;
}

method_environment* new_method_env(class_environment* class, char* return_type, char* *args, int n_args, char *name, node *root, int repeated_method) {

    method_environment *new_method = malloc(sizeof(method_environment));
    if(new_method == NULL) {
        printf("Error allocating memory");
    }
    new_method->father = class;
    new_method->return_type = strdup(return_type);
    new_method->arg_types = args;
    new_method->n_args = n_args;
    new_method->name = strdup(name);
    new_method->next = NULL;
    new_method->vars = NULL;
    new_method->root = root;
    new_method->repeated = repeated_method;


    return new_method;
}

class_environment* new_class_env(char *name) {
    class_environment *new_class = malloc(sizeof(class_environment));
    if(new_class == NULL) {
        printf("Error allocating memory");
    }

    new_class->name = strdup(name);
    new_class->methods = NULL;
    new_class->vars = NULL;
    new_class->order = NULL;

    return new_class;
}

int insert_method_var(char *name, char* type, method_environment *method, int is_param, int param_number) {
    //Returns 1 on success and 0 on insuccess

    table_element *cur = method->vars;

    if(cur == NULL) {
        table_element *new_var = new_table_element(name, type, is_param, 0, param_number);
        method->vars = new_var;
        return 1;
    }

    table_element *ant = cur;

    while(cur != NULL) {
        if(strcmp(cur->name, name) == 0) {
            return 0;
        }
        ant = cur;
        cur = cur->next;
    }

    table_element *new_var = new_table_element(name, type, is_param, 0, param_number);
    ant->next = new_var ;

    return 1;
}

int insert_class_var(char *name, char* type, class_environment *class) {

    table_element *cur = class->vars;

    if(cur == NULL) {
        table_element *new_var = new_table_element(name, type, 0, 1, -1);
        class->vars = new_var;
        insert_dec_order(0, class);
        return 1;
    }
    table_element *ant = cur;

    while(cur != NULL) {
        if(strcmp(cur->name, name) == 0){
            return 0;
        }
        ant = cur;
        cur = cur->next;
    }

    table_element *new_var = new_table_element(name, type, 0, 1, -1);
    ant->next = new_var;

    insert_dec_order(0, class);


    return 1;
}

int insert_class_method(char* return_type, char* *args, int n_args, char *name, class_environment* class, node *method_root) {
    //Returns 0 if it's repeated, 1 if not

    int repeated_method = 0;
    method_environment *cur = class->methods;
    if(cur == NULL) {
        method_environment *new_var = new_method_env(class, return_type, args, n_args, name, method_root, 0);
        class->methods = new_var;
        insert_dec_order(1, class);
        return 1;
    }
    method_environment *ant = cur;

    while(cur != NULL) {
        if(compare_methods(cur, name, args, n_args) == 1) {
            repeated_method = 1;
        }
        ant = cur;
        cur = cur->next;
    }

    method_environment *new_method = new_method_env(class, return_type, args, n_args, name, method_root, repeated_method);
    ant->next = new_method;

    if(repeated_method == 0) {
        insert_dec_order(1, class);
    }

    return repeated_method == 1 ? 0 : 1;
}

int compare_methods(method_environment *m1, char *name, char** args, int n_args) {
    if(strcmp(m1->name, name) != 0) {
        return 0;
    }
    if(m1->n_args != n_args) {
        return 0;
    }
    int i;
    for(i = 0; i < n_args; i++) {
        if(strcmp(args[i], m1->arg_types[i]) != 0) {
            return 0;
        }
    }
    return 1;
}

table_element *search_var(char *name, method_environment *method) {
    table_element *var = method->vars;

    while(var != NULL) {
        if(strcmp(var->name, name) == 0) {
            return var;
        }
        var = var->next;
    }

    var = method->father->vars;
    while(var != NULL) {
        if(strcmp(var->name, name) == 0) {
            return var;
        }
        var = var->next;
    }

    return NULL;
}


int search_method(char* name, method_environment *meth_env, char** arguments, int args_size){
  //Returns the index of the method, -1 if it doesnt exist and -2 if it's ambiguous

    method_environment *aux = meth_env->father->methods;

    int partial_found = 0;
    int partial_index = -1;

    int index = -1;

    while(aux != NULL) {
        index++;
        if(strcmp(aux->name, name)!=0 || args_size != aux->n_args) {
            aux = aux->next;
            continue;
        }
        int i;
        int cur_partial = 0;
        int not_found = 0;

        for(i = 0; i < aux->n_args; i++){

            if(strcmp(aux->arg_types[i], arguments[i]) == 0){
                continue;
            }
            else if (strcmp(aux->arg_types[i], "Double") == 0 && strcmp(arguments[i], "Int") == 0){
                cur_partial = 1;
                continue;
            }
            else {
                not_found = 1;
                cur_partial = 0;
                break;
            }
        }
        if(not_found == 1){
            aux = aux->next;
            continue;
        }
        else if(cur_partial == 1){
            partial_found++;
            partial_index = index;
            aux = aux->next;
            continue;
        }
        else{
            //Method totally matches the arguments
            return index;
        }
    }
    if(partial_found == 0) {
        return -1;
    }
    else if(partial_found == 1) {
        return partial_index;
    }
    return -2;
}


void print_env(class_environment *class_environment) {
    if(class_environment == NULL) {
        return;
    }
	printf("===== Class %s Symbol Table =====\n", class_environment->name);

    decl_order *order = class_environment->order;
    table_element *var = class_environment->vars;
    method_environment *cur = class_environment->methods;
    while(order != NULL) {
        if(order->is_method) {
            printf("%s\t(", cur->name);
            int i;
            for(i = 0; i < cur->n_args; i++) {
                if(i == 0) {
                    print_equivalent(cur->arg_types[i]);
                }
                else {
                    printf(",");
                    print_equivalent(cur->arg_types[i]);
                }
            }
            printf(")\t");
            print_equivalent(cur->return_type);
            printf("\n");
            cur = cur->next;
        }
        else {
            printf("%s\t\t", var->name);
            print_equivalent(var->type);
            printf("\n");
            var = var->next;
        }
        order = order->next;
    }

    cur = class_environment->methods;
    while(cur != NULL) {

        //print header part of the method
        printf("\n");
        printf("===== Method %s(", cur->name);
        int i;
        for(i = 0; i < cur->n_args; i++) {
            if(i == 0) {
                print_equivalent(cur->arg_types[i]);
            }
            else {
                printf(",");
                print_equivalent(cur->arg_types[i]);
            }
        }

        printf(") Symbol Table =====\n");

        //print return value
        printf("return\t\t");
        print_equivalent(cur->return_type);
        printf("\n");

        //print args
        var = cur->vars;
        while(var != NULL) {
            printf("%s\t\t", var->name);
            print_equivalent(var->type);
            if(var->is_param == 1) {
                printf("\tparam");
            }
            printf("\n");
            var = var->next;
        }

        cur = cur->next;
    }
    printf("\n");
}

void print_equivalent(char *type) {
    if(strcmp(type, "Undef") == 0) {
        printf("undef");
    }
    if(strcmp(type, "StringArray") == 0) {
        printf("String[]");
    }
    else if(strcmp(type, "Int") == 0) {
        printf("int");
    }
    else if(strcmp(type, "Double") == 0) {
        printf("double");
    }
    else if(strcmp(type, "Bool") == 0) {
        printf("boolean");
    }
    else if(strcmp(type, "Void") == 0) {
        printf("void");
    }
    else if(strcmp(type, "String") == 0) {
        printf("String");
    }
}

void free_linked_vars(table_element *var) {
    while(var != NULL) {
        table_element *cur = var;
        var = var->next;
        free(cur->name);
        free(cur);
    }
}

void free_class_env(class_environment *class) {
    if(class == NULL) {
        return;
    }
    free(class->name);
    method_environment *method = class->methods;
    while(method != NULL) {
        method_environment *cur = method;
        method = method->next;
        free_linked_vars(cur->vars);
        free(cur->name);
        free(cur->return_type);
        free(cur->arg_types);
        free(cur);
    }

    free_linked_vars(class->vars);
    decl_order *order = class->order;
    while(order != NULL) {
        decl_order *cur = order;
        order=cur->next;
        free(cur);
    }
    free(class);
    return;
}
