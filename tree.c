#include "tree.h"

extern int is_error;


node* new_node(char* name, char* value, int linha, int coluna){
	node* new_node = malloc(sizeof(node));
    if(new_node==NULL) {
        printf("Error alocating memory\n");
    }
	new_node->name = strdup(name);
    if(value == NULL) {
        new_node->value = NULL;
    } else {
	   new_node->value = value;
	}
    new_node->n_child = 0;
    new_node->child = NULL;
    new_node->anotation = NULL;

	new_node->method_anotation = NULL;
	new_node->return_anotation = NULL;

	new_node->linha = linha;
	new_node->coluna = coluna;
	new_node->table_el = NULL;
	return new_node;
}

void add_child(node* parent_ptr, node* child_ptr){
    parent_ptr->n_child++;
    parent_ptr->child = realloc(parent_ptr->child, parent_ptr->n_child*sizeof(node*));
    parent_ptr->child[parent_ptr->n_child-1] = child_ptr;
}

void free_tree(node* root) {
    int i;
    if(root==NULL) {
        return;
    }
    for(i=0; i<root->n_child; i++) {
        if(root->child[i] != NULL) {
            free_tree(root->child[i]);
        }
    }
    free(root->child);
	if(root->value != NULL) {
		free(root->value);
	}
	if(root->return_anotation != NULL) {
		free(root->return_anotation);
	}
    free(root->name);
    free(root);
}

void print_tree(node *root, int level) {
    if(root == NULL) {
        return;
    }
    int i=0;
    for(i=0; i<level-1; i++) {
        printf("..");
    }
    printf("%s", root->name);
    if(root->value != NULL) {
        printf("(%s)", root->value);
    }

    if(root->anotation != NULL) {
        printf(" - ");
        print_equivalent(root->anotation);

    }
    if(root->method_anotation != NULL){
        printf(" - (");
        for(i = 0; i < root->method_anotation->n_args; i++) {
            if(i == 0) {
                print_equivalent(root->method_anotation->arg_types[i]);
            }
            else {
                printf(",");
                print_equivalent(root->method_anotation->arg_types[i]);
            }
        }
        printf(")");

    }

    printf("\n");
    for(i=0; i<root->n_child; i++) {
        print_tree(root->child[i], level+1);
    }
}

void program_shallow_tree(node* program, node* sub_root) {
    if(sub_root == NULL || (strcmp(sub_root->name,"FieldDecl") != 0 && strcmp(sub_root->name, "MethodDecl")!=0 && strcmp(sub_root->name, "ProgramRec")!=0) ) {
        return;
    }
    else if(strcmp(sub_root->name,"ProgramRec") != 0) {
        add_child(program, sub_root);
    }
    int i;

    if(strcmp(sub_root->name, "FieldDecl") == 0) {
        field_decl_shallow_tree(program, sub_root->child[2], sub_root->child[0]);
        sub_root->child[2] = NULL;
    }

    if(strcmp(sub_root->name,"ProgramRec") == 0) {
        for(i=0; i < sub_root->n_child; i++) {
            program_shallow_tree(program, sub_root->child[i]);
        }
		if(sub_root->child != NULL) {
			free(sub_root->child);
		}
		free(sub_root->name);
		free(sub_root);
    }
}

void statement_shallow_tree(node* statement, node* sub_root){
    if(sub_root == NULL) {
        return;
	}
    else if(strcmp(sub_root->name, "StatementRec") != 0){
        return;
    }
    else {
        if(sub_root->child[0]!=NULL) {
            add_child(statement, sub_root->child[0]);
        }
        statement_shallow_tree(statement, sub_root->child[1]);
		if(sub_root->child != NULL) {
			free(sub_root->child);
		}
		free(sub_root->name);
		free(sub_root);
    }

}

void method_body_shallow_tree(node* method_body, node* sub_root) {

    int n_statements = 11;
    char *statements[n_statements];

    statements[0] = "Block";
    statements[1] = "DoWhile";
    statements[2] = "If";
    statements[3] = "Print";
    statements[4] = "Return";
    statements[5] = "While";
    statements[6] = "Assign";
    statements[7] = "Call";
    statements[8] = "ParseArgs";
    statements[9] = "MethodBodyRec";
    statements[10] = "VarDecl";

    int i = 0;
    int is_statement = 0;

    if(sub_root == NULL) {
        return;
    }
    for(i=0; i<n_statements; i++) {
        if(strcmp(statements[i],sub_root->name) == 0) {
            is_statement = 1;
            break;
        }
    }
    if(is_statement == 0) {
        return;
    }
    else {
        //Reached one of the statements
        if(strcmp(sub_root->name,"MethodBodyRec") != 0) {
            if(strcmp(sub_root->name, "Error")!=0) {
                add_child(method_body, sub_root);
            }
            if(strcmp(sub_root->name, "VarDecl") == 0) {
                //if it's a vardecl then we call var_decl_shallow_tree
                var_decl_shallow_tree(method_body, sub_root->child[2], sub_root->child[0]);
                sub_root->child[2]=NULL;
            }
        }
    }

    if(strcmp(sub_root->name,"MethodBodyRec") == 0) {
        for(i=0; i < sub_root->n_child; i++) {
            method_body_shallow_tree(method_body, sub_root->child[i]);
        }
    }
    if(strcmp(sub_root->name, "MethodBodyRec") == 0) {
        free(sub_root->name);
        free(sub_root->child);
        free(sub_root);
    }
}

void method_params_shallow_tree(node *method_params, node *sub_root) {
    if(sub_root == NULL) {
        return;
    }
    node* paramDecl = new_node("ParamDecl", NULL, sub_root->child[0]->linha, sub_root->child[0]->coluna);
    add_child(paramDecl, sub_root->child[0]);
    add_child(paramDecl, sub_root->child[1]);
    add_child(method_params, paramDecl);
    method_params_shallow_tree(method_params, sub_root->child[2]);
	if(sub_root->child != NULL) {
		free(sub_root->child);
	}
	free(sub_root->name);
	free(sub_root);
}

void field_decl_shallow_tree(node *program, node *sub_root, node *type) {
    if(sub_root == NULL) {
        return;
    }
    node *type_node = new_node(type->name, NULL, type->linha, type->coluna);
    node *id = sub_root->child[0];
    node *field_decl = new_node("FieldDecl", NULL, type->linha, type->coluna);

    add_child(field_decl, type_node);
    add_child(field_decl, id);
    add_child(program, field_decl);

    field_decl_shallow_tree(program, sub_root->child[1], type);
	if(sub_root->child != NULL) {
		free(sub_root->child);
	}
	free(sub_root->name);
    free(sub_root);

}

void var_decl_shallow_tree(node *method_body, node *sub_root, node *type) {
    if(sub_root == NULL) {
        return;
    }
    node *type_node = new_node(type->name, NULL, type->linha, type->coluna);
    node *id = sub_root->child[0];
    node *var_decl = new_node("VarDecl", NULL, type->linha, type->coluna);

    add_child(var_decl, type_node);
    add_child(var_decl, id);
    add_child(method_body, var_decl);

    var_decl_shallow_tree(method_body, sub_root->child[1], type);
	if(sub_root->child != NULL) {
		free(sub_root->child);
	}
	free(sub_root->name);
    free(sub_root);
}

void call_shallow_tree(node *call, node *sub_root) {
    if(sub_root == NULL) {
        return;
    }
    add_child(call, sub_root->child[0]);
    call_shallow_tree(call, sub_root->child[1]);
	if(sub_root->child != NULL) {
		free(sub_root->child);
	}
	free(sub_root->name);
	free(sub_root);
}

/********* SYMBOL TABLE ********/


class_environment* check_semantics(node *root, class_environment *class_env) {
	if(root == NULL) {
		return NULL;
	}
	char * class_name = root->child[0]->value;
	class_env = new_class_env(class_name);
	find_class_vars(root, class_env);
	check_program(root, class_env);
	return class_env;
}

//Gets function environment depending on an index (it's in an array list)
method_environment *get_method_environment(int i, class_environment *class_environment) {
 	method_environment *mt = class_environment->methods;

	int aux;
	for(aux = 0; aux < i; aux++) {
		if(mt == NULL) {
			printf("This souldn't happen... Get method environment function\n");
		}
		mt = mt->next;
	}
	return mt;
}

//Finds all variables in a class. Methods and Vars
void find_class_vars(node *root, class_environment *class_env) {
	int i;
	int n_methods = 0;
	for(i = 0; i < root->n_child; i++) {
		//root is "program". it's child can be field ou method decl
		if(root->child[i] == NULL) {
			continue;
		}
		else if(strcmp(root->child[i]->name, "FieldDecl") == 0) {
			//Insert var in the class environment
			if(insert_class_var(root->child[i]->child[1]->value, root->child[i]->child[0]->name, class_env) == 0) {
				is_error = 1;
				printf("Line %d, col %d: Symbol %s already defined\n", root->child[i]->child[1]->linha, root->child[i]->child[1]->coluna, root->child[i]->child[1]->value);
			}
		}
		else if(strcmp(root->child[i]->name, "MethodDecl") == 0) {
			node *method_header = root->child[i]->child[0];
			int n_args = get_method_n_args(method_header);
			//Insert a method environment in the class env
			char **args = get_method_args(method_header, n_args);

			int res = insert_class_method(get_method_return_type(method_header), args, n_args, get_method_name(method_header), class_env, root->child[i]);

			node* methodParams = method_header->child[2];
			method_environment *meth_env = get_method_environment(n_methods,class_env);

			int x, num_param = 0;
			for(x = 0; x < methodParams->n_child; x++) {
				node *paramDecl = methodParams->child[x];
				if(paramDecl == NULL) {
					continue;
				}
				char *name = paramDecl->child[1]->value;
				char *type = paramDecl->child[0]->name;
				int res = insert_method_var(name, type, meth_env, 1, num_param);
				if(res == 0) {
					is_error = 1;
					printf("Line %d, col %d: Symbol %s already defined\n", paramDecl->child[1]->linha, paramDecl->child[1]->coluna, name);
				}
				num_param++;
			}

			 if(res == 0){
				is_error = 1;
				printf("Line %d, col %d: Symbol %s(" , root->child[i]->child[0]->child[1]->linha, root->child[i]->child[0]->child[1]->coluna, root->child[i]->child[0]->child[1]->value);

				int u;
		        for(u = 0; u < n_args; u++) {
					if(u != 0) {
						printf(",");
					}
		            print_equivalent(args[u]);
		        }
				printf(") already defined\n");
			}
			n_methods++;
		}
	}
}

//Given a method header node returns its name
char *get_method_name(node *method_header) {
	return method_header->child[1]->value;
}

//Given a method header node returns its return type. It's somewhere in the tree
char* get_method_return_type(node *method_header) {
	return method_header->child[0]->name;
}

int get_method_n_args(node *method_header) {
	node *method_params = method_header->child[2];
	int i;
	int counter = 0;
	for(i = 0; i < method_params->n_child; i++) {
		if(method_params->child[i] == NULL) {
			continue;
		}
		counter++;
	}

	return counter;
}

char** get_method_args(node *method_header, int n_args) {
	char **args = malloc(n_args*sizeof(char*));
	int counter = 0, i;

	node *method_params = method_header->child[2];

	for(i = 0; i < method_params->n_child; i++) {
		if(method_params->child[i] == NULL) {
			continue;
		}
		args[counter++] = method_params->child[i]->child[0]->name;
	}

	return args;
}


/********* Árvore anotada *********/

function_type get_proper_function(char *node_name){
    //------ Terminais
    if (strcmp(node_name, "Int") == 0){ //Check
        return check_int;
    }
    else if(strcmp(node_name, "Bool") == 0){ //Check
        return check_bool;
    }
    else if (strcmp(node_name, "BoolLit") == 0){ //CHECK
        return check_boolLit;
    }
    else if (strcmp(node_name, "Double") == 0){ //CHECK
        return check_double;
    }
    else if (strcmp(node_name, "DecLit") == 0){ //CHECK
        return check_declit;
    }
    else if (strcmp(node_name, "Id") == 0){ //CHECK
        return check_id;
    }
    else if (strcmp(node_name, "RealLit") == 0){ //CHECK
        return check_realLit;
    }
    else if (strcmp(node_name, "StrLit") == 0){ //CHECK
        return check_strlLit;
    }
    else if (strcmp(node_name, "StringArray") == 0){ //CHECK
        return check_stringArray;
    }
    else if (strcmp(node_name, "Void") == 0){ //CHECK
        return check_void;
    }
    //-----Operadores
    else if (strcmp (node_name, "Assign") == 0){ //CHECK
        return check_assign;
    }
    else if (strcmp(node_name, "Or") == 0){ //CHECK
        return check_or;
    }
    else if (strcmp(node_name, "And") == 0){ //CHECK
        return check_and;
    }
    else if (strcmp(node_name, "Eq") == 0){ //CHECK
        return check_eq;
    }
    else if (strcmp(node_name, "Neq") == 0){ //CHECK
        return check_neq;
    }
    else if (strcmp(node_name, "Lt") == 0){ //CHECK
        return check_lt;
    }
    else if(strcmp(node_name, "Leq") == 0){ //CHECK
        return check_leq;
    }
	else if(strcmp(node_name, "Gt") == 0){ //CHECK
        return check_gt;
    }
    else if(strcmp(node_name, "Geq") == 0){ //CHECK
        return check_geq;
    }
    else if(strcmp(node_name, "Add") == 0){ //CHECK
        return check_add;
    }
    else if(strcmp(node_name, "Sub") == 0){ //CHECK
        return check_sub;
    }
    else if (strcmp(node_name, "Mul") == 0){ //CHECK
        return check_mul;
    }
    else if (strcmp(node_name, "Div") == 0){ //CHECK
        return check_div;
    }
    else if (strcmp(node_name, "Mod") == 0){ //CHECK
        return check_mod;
    }
    else if (strcmp(node_name, "Not") == 0){ //CHECK
        return check_not;
    }
    else if (strcmp(node_name, "Minus") == 0){ //CHECK
        return check_minus;
    }
    else if (strcmp(node_name, "Plus") == 0){ //CHECK
        return check_plus;
    }
    else if (strcmp(node_name, "Length") == 0){ //CHECK
        return check_length;
    }
    else if (strcmp(node_name, "Call") == 0){ //CHECK
        return check_call;
    }
    else if (strcmp(node_name, "ParseArgs") == 0){ //CHECK
        return check_parseArgs;
    }
    //-------- STATEMENTS
	else if(strcmp(node_name, "VarDecl") == 0) { //CHECK
		return check_varDecl;
	}
    else if (strcmp(node_name, "Block") == 0){ //CHECK
        return check_block;
    }
    else if (strcmp(node_name, "DoWhile") == 0){ //CHECK
        return check_doWhile;
    }
    else if (strcmp(node_name, "If") == 0){ //CHECK
        return check_if;
    }
    else if (strcmp(node_name, "Print") == 0){ //CHECK
        return check_print;
    }
    else if (strcmp(node_name, "Return") == 0){
        return check_return;
    }
    else if (strcmp(node_name, "While") == 0){ //CHECK
        return check_while;
    }
    return NULL;
}

int check_program(node *root, class_environment *class_env) {

	method_environment *method = class_env->methods;
	method_environment *ant = method;

	while(method != NULL) {

		check_methodDecl(method->root, method);
		if(method->repeated == 1) {
			ant->next = method->next;
			method = method->next;
			continue;
		}
		else {
			ant = method;
			method = method->next;
		}
	}

	return 1;
}

int check_methodDecl(node *methodDecl, method_environment *meth_env) {

	check_methodHeader(methodDecl->child[0], meth_env);
	check_methodBody(methodDecl->child[1], meth_env);

	return 1;
}

int check_methodHeader(node *methodHeader, method_environment *meth_env) {

	return 1;
}

int check_methodBody(node *methodBody, method_environment *meth_env) {
	int i;
	for(i = 0; i < methodBody->n_child; i++) {
		if(methodBody->child[i] == NULL) {
			continue;
		}
		get_proper_function(methodBody->child[i]->name)(methodBody->child[i], meth_env);
	}

	return 1;
}

int check_fieldDecl(node *fieldDecl) {
	return 1;
}

char *check_varDecl(node *varDecl, method_environment *meth_env) {
	char *name = varDecl->child[1]->value;
	char *type = varDecl->child[0]->name;

	int res = insert_method_var(name, type, meth_env, 0, -1);
	if(res == 0) {
		is_error = 1;
		printf("Line %d, col %d: Symbol %s already defined\n", varDecl->child[1]->linha, varDecl->child[1]->coluna, name);
	}

    return NULL;
}

char* check_while(node *while_, method_environment *meth_env){

	char *cond_type = get_proper_function(while_->child[0]->name)(while_->child[0], meth_env);
	if(strcmp(cond_type, "Bool") == 0) {

	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Incompatible type ", while_->child[0]->linha, while_->child[0]->coluna);
		print_equivalent(cond_type);
		printf(" in while statement\n");
	}

	get_proper_function(while_->child[1]->name)(while_->child[1], meth_env);

    return NULL;
}

char* check_return(node *return_, method_environment *meth_env){

	return_->return_anotation = strdup(meth_env->return_type);

    if (return_->n_child == 1){
		char *type = get_proper_function(return_->child[0]->name)(return_->child[0], meth_env);

		if(strcmp(meth_env->return_type, "Void") == 0) {
			is_error = 1;
			printf("Line %d, col %d: Incompatible type ", return_->child[0]->linha, return_->child[0]->coluna);
			print_equivalent(return_->child[0]->anotation);
			printf(" in return statement\n");
			return NULL;
		}

		else if(strcmp(type, meth_env->return_type) == 0
				|| (strcmp(type, "Int") == 0 && strcmp(meth_env->return_type, "Double") == 0)) {
		}
		else {
			is_error = 1;
			printf("Line %d, col %d: Incompatible type ", return_->child[0]->linha, return_->child[0]->coluna);
			print_equivalent(type);
			printf(" in return statement\n");
		}
		return NULL;
	}
	if(strcmp(meth_env->return_type, "Void") == 0) {}
	else {
		is_error = 1;
		printf("Line %d, col %d: Incompatible type void in return statement\n", return_->linha, return_->coluna);
	}

    return NULL;
}


char* check_print(node *print, method_environment *meth_env){

    char *type = get_proper_function(print->child[0]->name)(print->child[0], meth_env);

	if(strcmp(type, "Int") == 0 || strcmp(type, "Bool") == 0 || strcmp(type, "Double") == 0 || strcmp(type, "String") == 0) {

	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Incompatible type ", print->child[0]->linha, print->child[0]->coluna);
		print_equivalent(type);
		printf(" in System.out.println statement\n");
	}

    return type;
}

char* check_if(node *if_, method_environment *meth_env){

    char *cond_type = get_proper_function(if_->child[0]->name)(if_->child[0], meth_env);

	if(strcmp(cond_type, "Bool") == 0) {

	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Incompatible type ", if_->child[0]->linha, if_->child[0]->coluna);
		print_equivalent(cond_type);
		printf(" in if statement\n");
	}

	get_proper_function(if_->child[1]->name)(if_->child[1], meth_env);
	get_proper_function(if_->child[2]->name)(if_->child[2], meth_env);

    return NULL;
}

char* check_doWhile(node *doWhile, method_environment *meth_env){

	get_proper_function(doWhile->child[0]->name)(doWhile->child[0], meth_env);

	char *cond_type = get_proper_function(doWhile->child[1]->name)(doWhile->child[1], meth_env);
	if(strcmp(cond_type, "Bool") == 0) {

	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Incompatible type ", doWhile->child[1]->linha, doWhile->child[1]->coluna);
		print_equivalent(cond_type);
		printf(" in do statement\n");
	}

    return NULL;
}

char* check_block(node *block, method_environment *meth_env){

	int i;
	for (i = 0; i < block->n_child; i++){
        if (block->child[i] == NULL)
            continue;
        else
             get_proper_function(block->child[i]->name)(block->child[i], meth_env);
    }

    return NULL;
}

char* check_parseArgs(node *parseArgs, method_environment *meth_env){

  	char *type_1 = get_proper_function(parseArgs->child[0]->name)(parseArgs->child[0], meth_env);
	char *type_2 = get_proper_function(parseArgs->child[1]->name)(parseArgs->child[1], meth_env);

	if(strcmp(type_1, "StringArray") == 0 && strcmp(type_2, "Int") == 0) {

	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator Integer.parseInt cannot be applied to types ", parseArgs->linha, parseArgs->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
	}
    parseArgs->anotation = "Int";

    return parseArgs->anotation;
}

char* check_call(node *call, method_environment *meth_env){
    char *name = call->child[0]->value;
    int size = 0;
    int i;

	//Begins at child 1 because the first is the id (name of the method)
    for (i = 1; i < call->n_child; i++){
        if (call->child[i] == NULL)
            continue;
        else{
			get_proper_function(call->child[i]->name)(call->child[i], meth_env);
            size ++;
        }
    }
    char **arguments = malloc(sizeof(char*)*size);
	int aux = 0;
    for (i = 1; i < call->n_child; i++){
        if (call->child[i] == NULL)
            continue;
        else{
            arguments[aux++] = call->child[i]->anotation;
        }
    }

    int method_i = search_method(name, meth_env, arguments, size);

	if( method_i == -1) {
		is_error = 1;
		printf("Line %d, col %d: Cannot find symbol %s(", call->child[0]->linha, call->child[0]->coluna, call->child[0]->value);
		for(i = 0; i < size; i++) {
			if(i == 0) {
					print_equivalent(arguments[i]);
			}
			else {
					printf(",");
					print_equivalent(arguments[i]);
			}
		}
		printf(")\n");
		call->child[0]->anotation = "Undef";
		call ->anotation = "Undef";
		free(arguments);
		return "Undef";
	}
	else if(method_i == -2) {
		is_error = 1;
		printf("Line %d, col %d: Reference to method %s(", call->child[0]->linha, call->child[0]->coluna, call->child[0]->value);
		for(i = 0; i < size; i++) {
			if(i == 0) {
				print_equivalent(arguments[i]);
			}
			else {
				printf(",");
				print_equivalent(arguments[i]);
			}
		}
		printf(") is ambiguous\n");
		call->anotation = "Undef";
		call->child[0]->anotation = "Undef";
		free(arguments);
		return "Undef";
	}

	method_environment *method = get_method_environment(method_i, meth_env->father);

    call->anotation = get_method_return_type(method->root->child[0]);

    //Anotate son - method id
    node *method_id = call->child[0];
    method_id->method_anotation = method;
	free(arguments);
    return call->anotation;
}

char* check_length(node *length, method_environment *meth_env){

    char *type = get_proper_function(length->child[0]->name)(length->child[0], meth_env);

	if(strcmp(type, "StringArray") == 0) {

	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator .length cannot be applied to type ", length->linha, length->coluna+1);
		print_equivalent(type);
		printf("\n");
	}
	length->anotation = "Int";

    return length->anotation;
}

char* check_plus(node *plus, method_environment *meth_env){

    char *type = get_proper_function(plus->child[0]->name)(plus->child[0], meth_env);

	if(strcmp(type, "Double") == 0 || strcmp(type, "Int") == 0) {
		plus->anotation = type;
	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator + cannot be applied to type ", plus->linha, plus->coluna);
		print_equivalent(type);
		printf("\n");
		plus->anotation = "Undef";
	}

    return plus->anotation;
}

char* check_minus(node *minus, method_environment *meth_env){
    char *type = get_proper_function(minus->child[0]->name)(minus->child[0], meth_env);

	if(strcmp(type, "Double") == 0 || strcmp(type, "Int") == 0) {
		minus->anotation = type;
	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator - cannot be applied to type ", minus->linha, minus->coluna);
		print_equivalent(type);
		printf("\n");
		minus->anotation = "Undef";
	}

    return minus->anotation;
}


char* check_not(node *not, method_environment *meth_env){

    char *type = get_proper_function(not->child[0]->name)(not->child[0], meth_env);

	if(strcmp(type, "Bool") != 0) {
		is_error = 1;
		printf("Line %d, col %d: Operator ! cannot be applied to type ", not->linha, not->coluna);
		print_equivalent(type);
		printf("\n");
	}

	not->anotation = "Bool";

    return not->anotation;
}

char* check_mod(node *mod, method_environment *meth_env){

    char *type_1 = get_proper_function(mod->child[0]->name)(mod->child[0], meth_env);
    char *type_2 = get_proper_function(mod->child[1]->name)(mod->child[1], meth_env);

    if (strcmp(type_1, "Int") == 0 && strcmp(type_2, "Int") == 0){
        mod->anotation = "Int";
    }
	else if ((strcmp(type_1, "Double") == 0 && strcmp(type_2, "Int") == 0) || (strcmp(type_1, "Int") == 0 && strcmp(type_2, "Double") == 0) || (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Double") == 0)){

		mod->anotation = "Double";
    }
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator %% cannot be applied to types ", mod->linha, mod->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
		mod->anotation = "Undef";
	}

    return mod->anotation;
}

char* check_div(node *div_, method_environment *meth_env){

    char *type_1 = get_proper_function(div_->child[0]->name)(div_->child[0], meth_env);
    char *type_2 = get_proper_function(div_->child[1]->name)(div_->child[1], meth_env);

    if (strcmp(type_1, "Int") == 0 && strcmp(type_2, "Int") == 0){
        div_->anotation = type_2;
    }

    else if (strcmp(type_1, "Int") == 0 && strcmp(type_2, "Double") == 0){
        div_->anotation = type_2;
    }

    else if (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Int") == 0){
        div_->anotation = type_1;
    }
	else if (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Double") == 0){
		div_->anotation = type_1;
	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator / cannot be applied to types ", div_->linha, div_->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
		div_->anotation = "Undef";
	}

    return div_->anotation;
}

char* check_mul(node *mul, method_environment *meth_env){

    char *type_1 = get_proper_function(mul->child[0]->name)(mul->child[0], meth_env);
    char *type_2 = get_proper_function(mul->child[1]->name)(mul->child[1], meth_env);

    if (strcmp(type_1, "Int") == 0 && strcmp(type_2, "Int") == 0){
        mul->anotation = type_2;
    }

    else if (strcmp(type_1, "Int") == 0 && strcmp(type_2, "Double") == 0){
        mul->anotation = type_2;
    }

    else if (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Int") == 0){
        mul->anotation = type_1;
    }
	else if (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Double") == 0){
        mul->anotation = type_1;
    }
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator * cannot be applied to types ", mul->linha, mul->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
		mul->anotation = "Undef";
	}


    return mul->anotation;
}

char* check_sub(node *sub, method_environment *meth_env){

    char *type_1 = get_proper_function(sub->child[0]->name)(sub->child[0], meth_env);
    char *type_2 = get_proper_function(sub->child[1]->name)(sub->child[1], meth_env);

    if (strcmp(type_1, "Int") == 0 && strcmp(type_2, "Int") == 0){
        sub->anotation = type_2;
    }

    else if (strcmp(type_1, "Int") == 0 && strcmp(type_2, "Double") == 0){
        sub->anotation = type_2;
    }

    else if (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Int") == 0){
        sub->anotation = type_1;
    }

	else if (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Double") == 0){
        sub->anotation = type_1;
    }
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator - cannot be applied to types ", sub->linha, sub->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
		sub->anotation = "Undef";
	}

    return sub->anotation;
}

char* check_add(node *add, method_environment *meth_env){

    char *type_1 = get_proper_function(add->child[0]->name)(add->child[0], meth_env);
    char *type_2 = get_proper_function(add->child[1]->name)(add->child[1], meth_env);

    if (strcmp(type_1, "Int") == 0 && strcmp(type_2, "Int") == 0){
        add->anotation = type_2;
    }

    else if (strcmp(type_1, "Int") == 0 && strcmp(type_2, "Double") == 0){
        add->anotation = type_2;
    }

    else if (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Int") == 0){
        add->anotation = type_1;
    }
	else if (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Double") == 0){
        add->anotation = type_1;
    }
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator + cannot be applied to types ", add->linha, add->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
		add->anotation = "Undef";
	}

    return add->anotation;
}

char* check_gt(node *gt, method_environment *meth_env){

    char *type_1 = get_proper_function(gt->child[0]->name)(gt->child[0], meth_env);
    char *type_2 = get_proper_function(gt->child[1]->name)(gt->child[1], meth_env);

	gt->anotation = "Bool";

	if(check_arithmetic_congruency(type_1, type_2) == 1) {

	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator > cannot be applied to types ", gt->linha, gt->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
	}

    return gt->anotation;
}

char* check_geq(node *geq, method_environment *meth_env){

    char *type_1 = get_proper_function(geq->child[0]->name)(geq->child[0], meth_env);
    char *type_2 = get_proper_function(geq->child[1]->name)(geq->child[1], meth_env);

	geq->anotation = "Bool";

	if(check_arithmetic_congruency(type_1, type_2) == 1) {

	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator >= cannot be applied to types ", geq->linha, geq->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
	}

    return geq->anotation;
}

char* check_leq(node *leq, method_environment *meth_env){

	char *type_1 = get_proper_function(leq->child[0]->name)(leq->child[0], meth_env);
    char *type_2 = get_proper_function(leq->child[1]->name)(leq->child[1], meth_env);

	leq->anotation = "Bool";
	if(check_arithmetic_congruency(type_1, type_2) == 1) {

	}
	else {

		is_error = 1;
		printf("Line %d, col %d: Operator <= cannot be applied to types ", leq->linha, leq->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
	}
    return leq->anotation;
}

char* check_lt(node *lt, method_environment *meth_env){

	char *type_1 = get_proper_function(lt->child[0]->name)(lt->child[0], meth_env);
	char *type_2 = get_proper_function(lt->child[1]->name)(lt->child[1], meth_env);

	lt->anotation = "Bool";

	if(check_arithmetic_congruency(type_1, type_2) == 1) {

	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator < cannot be applied to types ", lt->linha, lt->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
	}

	return lt->anotation;
}

char* check_neq(node *neq, method_environment *meth_env){

	char *type_1 = get_proper_function(neq->child[0]->name)(neq->child[0], meth_env);
	char *type_2 = get_proper_function(neq->child[1]->name)(neq->child[1], meth_env);

	neq->anotation = "Bool";

	if(check_comparison_congruency(type_1, type_2) == 1) {

	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator != cannot be applied to types ", neq->linha, neq->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
	}

	return neq->anotation;
}

char* check_eq(node *eq, method_environment *meth_env){

	char *type_1 = get_proper_function(eq->child[0]->name)(eq->child[0], meth_env);
	char *type_2 = get_proper_function(eq->child[1]->name)(eq->child[1], meth_env);

	eq->anotation = "Bool";

	if(check_comparison_congruency(type_1, type_2) == 1) {

	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator == cannot be applied to types ", eq->linha, eq->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
	}

	return eq->anotation;
}

char* check_and(node *and, method_environment *meth_env){

	char *type_1 = get_proper_function(and->child[0]->name)(and->child[0], meth_env);
	char *type_2 = get_proper_function(and->child[1]->name)(and->child[1], meth_env);

	and->anotation = "Bool";

	if(strcmp(type_1, "Bool") == 0 && strcmp(type_2, "Bool") == 0) {

	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator && cannot be applied to types ", and->linha, and->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
	}

	return and->anotation;
}

char* check_or(node *or, method_environment *meth_env){

	char *type_1 = get_proper_function(or->child[0]->name)(or->child[0], meth_env);
	char *type_2 = get_proper_function(or->child[1]->name)(or->child[1], meth_env);

	or->anotation = "Bool";

	if(strcmp(type_1, "Bool") == 0 && strcmp(type_2, "Bool") == 0) {

	}
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator || cannot be applied to types ", or->linha, or->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
	}

	return or->anotation;
}

char* check_assign(node *assign, method_environment *meth_env) {
    //char *name = assign->child[0]->value;

   // table_element *var = search_var(name, meth_env);

    char *type_1 = get_proper_function(assign->child[0]->name)(assign->child[0], meth_env);
    char *type_2 = get_proper_function(assign->child[1]->name)(assign->child[1], meth_env);

    if (strcmp(type_1, "Int") == 0 && strcmp(type_2, "Int") == 0){
        assign->anotation = type_1;
    }

    else if (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Int") == 0){
        assign->anotation = type_1;
    }

	else if (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Double") == 0){
        assign->anotation = type_1;
    }

    else if (strcmp(type_1, "Bool") == 0 && strcmp(type_2, "Bool") == 0){
        assign->anotation = type_1;
    }
	else {
		is_error = 1;
		printf("Line %d, col %d: Operator = cannot be applied to types ", assign->linha, assign->coluna);
		print_equivalent(type_1);
		printf(", ");
		print_equivalent(type_2);
		printf("\n");
		assign->anotation = type_1;
	}

    return assign->anotation;

}


//------------ TERMINAIS

char* check_void(node* void_, method_environment *meth_env){
    char *name = void_->name;

    return name;
}

char* check_stringArray(node *StringArray, method_environment *meth_env){
    char *name = StringArray->name;

	StringArray->anotation = name;

    return name;
}

char* check_strlLit(node *strllit, method_environment *meth_env){

	strllit->anotation = "String";

    return "String";
}

char* check_realLit(node *reallit, method_environment *meth_env){

    char *number = reallit->value;
    char *aux = (char*)malloc(sizeof(char)*1024);
    int i = 0, j = 0, expoente = 0, k = 1;

    while(number[i] != '\0'){
        if(number[i] == 'E' || number[i] == 'e' || number[i] == '.' || number[i] == '-' || (number[i] >= '0' && number[i] <= '9')){
            if (number[i] == 'E' || number[i] == 'e'){
                expoente = 1;
            }
            if(number[i] != '.' && number[i] != '0' && !expoente){
                k = 0;
            }
            aux[j] = number[i];
            j++;
        }
        i++;
    }

    aux[j] = '\0';


    if(!k){
        double number_aux = atof(aux);
        if(number_aux > DBL_MAX || isinf(number_aux) || number_aux == 0 ){
			is_error = 1;
            printf("Line %d, col %d: Number %s out of bounds\n", reallit->linha, reallit->coluna, reallit->value);
        }
    }

    free(aux);
	reallit->anotation = "Double";

    return "Double";
}

char* check_int(node *integer, method_environment *meth_env){
    char *name = integer->name;

    return name;
}

char* check_id(node *id, method_environment *meth_env) {

	table_element *element = search_var(id->value, meth_env);

	if(element == NULL) {
		is_error = 1;
		printf("Line %d, col %d: Cannot find symbol %s\n", id->linha, id->coluna, id->value);
		id->anotation = "Undef";
		return id->anotation;
	}

	id->anotation = element->type;
	id->table_el = element;

    return id->anotation;
}



char* check_declit(node *declit, method_environment *meth_env){

	declit->anotation = "Int";

    long number = strtol(declit->value, NULL, 10);

    if (number > INT_MAX || number < INT_MIN){
		is_error = 1;
        printf("Line %d, col %d: Number %s out of bounds\n", declit->linha, declit->coluna, declit->value);
    }

    return "Int";
}

char* check_double(node *double_, method_environment *meth_env){
    char *name = double_->name;

    return name;
}

char* check_boolLit(node *boolLit, method_environment *meth_env){

	boolLit->anotation = "Bool";

    return "Bool";
}

char* check_bool(node *boolean, method_environment *meth_env){
    char *name = boolean->name;

    return name;
}

int check_arithmetic_congruency(char *type_1, char *type_2) {
	if ((strcmp(type_1, "Int") == 0 && strcmp(type_2, "Int") == 0)
		|| (strcmp(type_1, "Int") == 0 && strcmp(type_2, "Double") == 0)
		|| (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Int") == 0)
		|| (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Double") == 0)) {

		return 1;
	}
	return 0;
}

int check_comparison_congruency(char *type_1, char *type_2) {
	if ((strcmp(type_1, "Int") == 0 && strcmp(type_2, "Int") == 0)
		|| (strcmp(type_1, "Int") == 0 && strcmp(type_2, "Double") == 0)
		|| (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Int") == 0)
		|| (strcmp(type_1, "Double") == 0 && strcmp(type_2, "Double") == 0)
		|| (strcmp(type_1, "Bool") == 0 && strcmp(type_2, "Bool") == 0)) {

		return 1;
	}
	return 0;

}
