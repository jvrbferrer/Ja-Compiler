/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "jac.y" /* yacc.c:339  */

    #include "tree.h"
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    int yylex(void);
    void yyerror (const char *s);
    extern int printTokens;
    extern int justLexical;
    int tree_flag = 0;
    int table_flag = 0;
    int yylex_destroy(void);
    node* root;
    int is_error = 0;

    extern int yyleng;

    extern int linha;
    extern int coluna;

#line 87 "y.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif


/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    STRLIT = 258,
    RESERVED = 259,
    ID = 260,
    BOOLLIT = 261,
    REALLIT = 262,
    DECLIT = 263,
    ASSIGN = 264,
    SEMI = 265,
    COMMA = 266,
    WHILE = 267,
    DO = 268,
    PUBLIC = 269,
    STATIC = 270,
    CLASS = 271,
    PRINT = 272,
    DOTLENGTH = 273,
    PARSEINT = 274,
    RETURN = 275,
    INT = 276,
    BOOL = 277,
    DOUBLE = 278,
    STRING = 279,
    VOID = 280,
    ELSE = 281,
    IF = 282,
    PLUS = 283,
    MINUS = 284,
    STAR = 285,
    DIV = 286,
    MOD = 287,
    GEQ = 288,
    LEQ = 289,
    NEQ = 290,
    EQ = 291,
    LT = 292,
    GT = 293,
    OR = 294,
    AND = 295,
    NOT = 296,
    OSQUARE = 297,
    CSQUARE = 298,
    OBRACE = 299,
    CBRACE = 300,
    OCURV = 301,
    CCURV = 302,
    PRECEDENCE = 303,
    PLUSPREC = 304,
    NO_ELSE = 305
  };
#endif
/* Tokens.  */
#define STRLIT 258
#define RESERVED 259
#define ID 260
#define BOOLLIT 261
#define REALLIT 262
#define DECLIT 263
#define ASSIGN 264
#define SEMI 265
#define COMMA 266
#define WHILE 267
#define DO 268
#define PUBLIC 269
#define STATIC 270
#define CLASS 271
#define PRINT 272
#define DOTLENGTH 273
#define PARSEINT 274
#define RETURN 275
#define INT 276
#define BOOL 277
#define DOUBLE 278
#define STRING 279
#define VOID 280
#define ELSE 281
#define IF 282
#define PLUS 283
#define MINUS 284
#define STAR 285
#define DIV 286
#define MOD 287
#define GEQ 288
#define LEQ 289
#define NEQ 290
#define EQ 291
#define LT 292
#define GT 293
#define OR 294
#define AND 295
#define NOT 296
#define OSQUARE 297
#define CSQUARE 298
#define OBRACE 299
#define CBRACE 300
#define OCURV 301
#define CCURV 302
#define PRECEDENCE 303
#define PLUSPREC 304
#define NO_ELSE 305

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 22 "jac.y" /* yacc.c:355  */

    struct token_val_ val;
    struct Node* node_ptr;

#line 229 "y.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);



/* Copy the second part of user declarations.  */

#line 246 "y.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   384

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  51
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  26
/* YYNRULES -- Number of rules.  */
#define YYNRULES  83
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  180

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   305

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint8 yyrline[] =
{
       0,    64,    64,    66,    67,    68,    69,    71,    72,    74,
      75,    77,    79,    80,    82,    84,    85,    86,    88,    89,
      94,   100,   101,   103,   105,   106,   108,   109,   110,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     133,   144,   145,   147,   149,   150,   153,   154,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   182,   183,   185,   187,   189,   191,   192,
     194,   195,   197,   198
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "STRLIT", "RESERVED", "ID", "BOOLLIT",
  "REALLIT", "DECLIT", "ASSIGN", "SEMI", "COMMA", "WHILE", "DO", "PUBLIC",
  "STATIC", "CLASS", "PRINT", "DOTLENGTH", "PARSEINT", "RETURN", "INT",
  "BOOL", "DOUBLE", "STRING", "VOID", "ELSE", "IF", "PLUS", "MINUS",
  "STAR", "DIV", "MOD", "GEQ", "LEQ", "NEQ", "EQ", "LT", "GT", "OR", "AND",
  "NOT", "OSQUARE", "CSQUARE", "OBRACE", "CBRACE", "OCURV", "CCURV",
  "PRECEDENCE", "PLUSPREC", "NO_ELSE", "$accept", "program", "programRec",
  "fieldDecl", "fieldDeclRec", "methodDecl", "methodHeader", "methodBody",
  "methodBodyRec", "formalParams", "formalParamsRec", "varDecl",
  "varDeclRec", "typeRule", "statement", "strlit", "statementRec",
  "expressionAux", "expression", "parseArgs", "assignment", "id", "void",
  "methodInvocation", "params", "paramsRec", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305
};
# endif

#define YYPACT_NINF -28

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-28)))

#define YYTABLE_NINF -81

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      -9,     5,    21,   -28,   -18,   -28,     3,    23,     3,    30,
     -25,     3,     3,   -28,   -28,   100,   -28,   -28,   -28,   -28,
     -28,   -28,   -28,     8,     5,     5,   228,   -28,    -8,    15,
      39,   -28,    22,   264,    25,    29,   297,    40,   251,    44,
     228,     5,   228,    81,    94,    -4,    95,     5,   120,   116,
     120,   -28,   312,   108,   282,     7,   -28,   -28,   -28,   -28,
     312,   312,   312,   198,   118,   331,   -28,   -28,    10,   -28,
     312,   251,    93,   -28,   -28,   128,   -28,   -28,   -28,   312,
     190,   -28,   137,   111,   105,     5,   -28,   109,   112,   114,
     -28,   117,   119,   123,   121,   -28,    12,   -28,   -28,   124,
     126,   -28,   312,   312,   312,   312,   312,   312,   312,   312,
     312,   312,   312,   312,   312,   -28,   127,   -28,   -28,     5,
     148,   -28,   129,   150,   145,   -28,   122,   -28,   156,   -28,
     264,   312,   183,   184,   -28,   312,   -28,   -28,    50,    50,
     -28,   -28,   -28,    35,    35,    64,    64,    35,    35,   344,
      79,   264,   128,   -28,   -28,   312,   -28,   -28,     5,   110,
     -28,   -28,   153,   -28,   -28,   158,   176,   -28,   150,   -28,
       5,   197,   161,   264,   -28,   156,   -28,   -28,   -28,   -28
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     0,    76,     0,     1,     0,     0,     0,     0,
       0,     0,     0,     8,     5,     0,     2,     3,     4,    27,
      26,    28,    77,     0,     0,     0,     0,    11,    10,     0,
       0,    31,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    18,     0,
      18,    42,     0,     0,     0,     0,    48,    50,    49,    29,
       0,     0,     0,     0,     0,    47,    71,    46,    68,    70,
       0,     0,     0,    14,    15,    24,    16,    34,    32,     0,
       0,    33,    10,     0,     0,     0,     7,     0,     0,     0,
      43,     0,     0,     0,     0,    52,    68,    53,    54,     0,
       0,    30,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    69,     0,    45,    41,     0,
       0,    75,     0,    82,     0,     9,     0,    12,    21,    13,
       0,     0,     0,     0,    74,     0,    72,    51,    55,    56,
      57,    58,    59,    61,    63,    65,    60,    64,    62,    67,
      66,     0,    24,    23,    79,     0,    81,    78,     0,     0,
      20,    38,     0,    36,    35,     0,    40,    25,    82,    19,
       0,     0,     0,     0,    83,    21,    37,    73,    39,    22
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -28,   -28,    65,   -28,   130,   -28,   -28,   -28,    45,   160,
      36,   -28,    61,   -14,   -27,   -28,   143,    20,    75,   -24,
     -11,    -1,   -28,    17,   -28,    47
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,    10,    11,    49,    12,    23,    27,    39,    84,
     160,    40,   120,    41,    42,    91,    72,    64,    65,    66,
      67,    96,    25,    69,   124,   156
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
       4,    24,    43,    47,     7,    79,    53,     1,    93,    43,
       3,    71,     3,     8,    43,    44,    43,     9,    43,    79,
      16,     5,    44,    28,    29,    45,     6,    44,   115,    44,
     115,    44,    45,    13,    85,    68,    85,    45,    48,    45,
      75,    45,    80,    46,    71,    15,    82,    43,    -6,    51,
      46,    68,    26,    68,    94,    46,    80,    46,    80,    46,
      44,    50,    68,   102,   103,   104,   105,   106,    52,    68,
      45,    54,    88,    14,    92,    55,    17,    18,    68,    68,
     104,   105,   106,   100,   128,    74,    70,    76,    46,    73,
     116,    77,   102,   103,   104,   105,   106,   107,   108,   121,
     123,   111,   112,   161,    78,    81,    43,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   152,    44,
      89,    19,    20,    21,   166,    22,    86,    43,   101,    45,
      68,    19,    20,    21,    68,    95,    97,    98,   118,   119,
      44,    19,    20,    21,    83,   170,   178,    46,    47,    43,
      45,   162,   127,   126,    68,   165,   129,   169,   153,   130,
     131,   155,    44,   135,   132,   158,   133,   159,    46,   175,
     134,   136,    45,   137,   151,   168,   154,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
      46,   122,   157,   163,   164,     3,    56,    57,    58,    99,
     171,   172,   173,     3,    56,    57,    58,   176,   177,    35,
      87,   179,   125,   167,   117,   174,     0,    35,    60,    61,
       0,     0,     0,     0,     0,     0,    60,    61,     0,    30,
       0,    62,     0,     3,     0,     0,    63,   -80,    31,    62,
      32,    33,     0,     0,    63,    34,     0,    35,    36,    19,
      20,    21,    30,     0,     0,    37,     3,     0,     0,     0,
       0,    31,     0,    32,    33,    30,     0,     0,    34,     3,
      35,    36,    38,   -17,    31,     0,    32,    33,    37,     0,
       0,    34,     0,    35,    36,    90,     0,     3,    56,    57,
      58,    37,     0,     0,     0,    38,   -44,     0,     0,     0,
       0,    35,     3,    56,    57,    58,     0,    59,    38,     0,
      60,    61,     0,     0,     0,     0,    35,     3,    56,    57,
      58,     0,     0,    62,     0,    60,    61,     0,    63,     0,
       0,    35,     0,     0,     0,     0,     0,     0,    62,     0,
      60,    61,     0,    63,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    62,     0,     0,     0,     0,    63,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,     0,   114
};

static const yytype_int16 yycheck[] =
{
       1,    15,    26,    11,     1,     9,    33,    16,     1,    33,
       5,    38,     5,    10,    38,    26,    40,    14,    42,     9,
      45,     0,    33,    24,    25,    26,    44,    38,    18,    40,
      18,    42,    33,    10,    48,    36,    50,    38,    46,    40,
      41,    42,    46,    26,    71,    15,    47,    71,    45,    10,
      33,    52,    44,    54,    55,    38,    46,    40,    46,    42,
      71,    46,    63,    28,    29,    30,    31,    32,    46,    70,
      71,    46,    52,     8,    54,    46,    11,    12,    79,    80,
      30,    31,    32,    63,    85,    40,    46,    42,    71,    45,
      70,    10,    28,    29,    30,    31,    32,    33,    34,    79,
      80,    37,    38,   130,    10,    10,   130,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,   119,   130,
      12,    21,    22,    23,   151,    25,    10,   151,    10,   130,
     131,    21,    22,    23,   135,    60,    61,    62,    45,    11,
     151,    21,    22,    23,    24,   159,   173,   130,    11,   173,
     151,   131,    47,    42,   155,   135,    47,   158,    10,    47,
      46,    11,   173,    42,    47,    43,    47,    11,   151,   170,
      47,    47,   173,    47,    47,   155,    47,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     173,     1,    47,    10,    10,     5,     6,     7,     8,     1,
      47,    43,    26,     5,     6,     7,     8,    10,    47,    19,
      50,   175,    82,   152,    71,   168,    -1,    19,    28,    29,
      -1,    -1,    -1,    -1,    -1,    -1,    28,    29,    -1,     1,
      -1,    41,    -1,     5,    -1,    -1,    46,    47,    10,    41,
      12,    13,    -1,    -1,    46,    17,    -1,    19,    20,    21,
      22,    23,     1,    -1,    -1,    27,     5,    -1,    -1,    -1,
      -1,    10,    -1,    12,    13,     1,    -1,    -1,    17,     5,
      19,    20,    44,    45,    10,    -1,    12,    13,    27,    -1,
      -1,    17,    -1,    19,    20,     3,    -1,     5,     6,     7,
       8,    27,    -1,    -1,    -1,    44,    45,    -1,    -1,    -1,
      -1,    19,     5,     6,     7,     8,    -1,    10,    44,    -1,
      28,    29,    -1,    -1,    -1,    -1,    19,     5,     6,     7,
       8,    -1,    -1,    41,    -1,    28,    29,    -1,    46,    -1,
      -1,    19,    -1,    -1,    -1,    -1,    -1,    -1,    41,    -1,
      28,    29,    -1,    46,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    41,    -1,    -1,    -1,    -1,    46,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    -1,    40
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    16,    52,     5,    72,     0,    44,     1,    10,    14,
      53,    54,    56,    10,    53,    15,    45,    53,    53,    21,
      22,    23,    25,    57,    64,    73,    44,    58,    72,    72,
       1,    10,    12,    13,    17,    19,    20,    27,    44,    59,
      62,    64,    65,    70,    71,    72,    74,    11,    46,    55,
      46,    10,    46,    65,    46,    46,     6,     7,     8,    10,
      28,    29,    41,    46,    68,    69,    70,    71,    72,    74,
      46,    65,    67,    45,    59,    72,    59,    10,    10,     9,
      46,    10,    72,    24,    60,    64,    10,    60,    68,    12,
       3,    66,    68,     1,    72,    69,    72,    69,    69,     1,
      68,    10,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    18,    68,    67,    45,    11,
      63,    68,     1,    68,    75,    55,    42,    47,    72,    47,
      47,    46,    47,    47,    47,    42,    47,    47,    69,    69,
      69,    69,    69,    69,    69,    69,    69,    69,    69,    69,
      69,    47,    72,    10,    47,    11,    76,    47,    43,    11,
      61,    65,    68,    10,    10,    68,    65,    63,    68,    72,
      64,    47,    43,    26,    76,    72,    10,    47,    65,    61
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    51,    52,    53,    53,    53,    53,    54,    54,    55,
      55,    56,    57,    57,    58,    59,    59,    59,    60,    60,
      60,    61,    61,    62,    63,    63,    64,    64,    64,    65,
      65,    65,    65,    65,    65,    65,    65,    65,    65,    65,
      65,    65,    65,    66,    67,    67,    68,    68,    69,    69,
      69,    69,    69,    69,    69,    69,    69,    69,    69,    69,
      69,    69,    69,    69,    69,    69,    69,    69,    69,    69,
      69,    69,    69,    70,    70,    71,    72,    73,    74,    74,
      75,    75,    76,    76
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     5,     2,     2,     2,     0,     6,     2,     3,
       0,     4,     5,     5,     3,     2,     2,     0,     0,     4,
       3,     0,     4,     4,     0,     3,     1,     1,     1,     2,
       3,     1,     2,     2,     2,     5,     5,     7,     5,     7,
       5,     3,     2,     1,     0,     2,     1,     1,     1,     1,
       1,     3,     2,     2,     2,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     1,     2,
       1,     1,     3,     7,     4,     3,     1,     1,     4,     4,
       0,     2,     0,     3
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 64 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Program", NULL, (yyvsp[-3].node_ptr)->linha, (yyvsp[-3].node_ptr)->coluna); add_child((yyval.node_ptr), (yyvsp[-3].node_ptr)); program_shallow_tree((yyval.node_ptr), (yyvsp[-1].node_ptr));root=(yyval.node_ptr);}
#line 1486 "y.tab.c" /* yacc.c:1646  */
    break;

  case 3:
#line 66 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("ProgramRec", NULL, (yyvsp[-1].node_ptr)->linha, (yyvsp[-1].node_ptr)->coluna); add_child((yyval.node_ptr), (yyvsp[-1].node_ptr)); add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1492 "y.tab.c" /* yacc.c:1646  */
    break;

  case 4:
#line 67 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("ProgramRec", NULL, (yyvsp[-1].node_ptr)->linha, (yyvsp[-1].node_ptr)->coluna); add_child((yyval.node_ptr), (yyvsp[-1].node_ptr)); add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1498 "y.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 68 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("ProgramRec", NULL, 0, 0); add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1504 "y.tab.c" /* yacc.c:1646  */
    break;

  case 6:
#line 69 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = NULL;}
#line 1510 "y.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 71 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("FieldDecl", NULL, (yyvsp[-2].node_ptr)->linha, (yyvsp[-2].node_ptr)->coluna);add_child((yyval.node_ptr), (yyvsp[-3].node_ptr)); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr)); add_child((yyval.node_ptr), (yyvsp[-1].node_ptr));}
#line 1516 "y.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 72 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Error", NULL,(yyvsp[0].val).linha,(yyvsp[0].val).coluna);tree_flag=0;}
#line 1522 "y.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 74 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("FieldDeclRec", NULL, (yyvsp[-1].node_ptr)->linha, (yyvsp[-1].node_ptr)->coluna); add_child((yyval.node_ptr), (yyvsp[-1].node_ptr)); add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1528 "y.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 75 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = NULL;}
#line 1534 "y.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 77 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("MethodDecl", NULL, (yyvsp[-3].val).linha, (yyvsp[-3].val).coluna); add_child((yyval.node_ptr), (yyvsp[-1].node_ptr)); add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1540 "y.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 79 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("MethodHeader", NULL, (yyvsp[-4].node_ptr)->linha, (yyvsp[-4].node_ptr)->coluna); add_child((yyval.node_ptr), (yyvsp[-4].node_ptr)); add_child((yyval.node_ptr), (yyvsp[-3].node_ptr)); add_child((yyval.node_ptr), (yyvsp[-1].node_ptr));}
#line 1546 "y.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 80 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("MethodHeader", NULL, (yyvsp[-4].node_ptr)->linha, (yyvsp[-4].node_ptr)->coluna); add_child((yyval.node_ptr), (yyvsp[-4].node_ptr)); add_child((yyval.node_ptr), (yyvsp[-3].node_ptr)); add_child((yyval.node_ptr), (yyvsp[-1].node_ptr));}
#line 1552 "y.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 82 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("MethodBody", NULL, (yyvsp[-2].val).linha, (yyvsp[-2].val).coluna); method_body_shallow_tree((yyval.node_ptr), (yyvsp[-1].node_ptr));}
#line 1558 "y.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 84 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("MethodBodyRec", NULL, (yyvsp[-1].node_ptr)->linha, (yyvsp[-1].node_ptr)->coluna); add_child((yyval.node_ptr), (yyvsp[-1].node_ptr)); add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1564 "y.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 85 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("MethodBodyRec", NULL, 0, 0); add_child((yyval.node_ptr), (yyvsp[-1].node_ptr)); add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1570 "y.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 86 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = NULL;}
#line 1576 "y.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 88 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("MethodParams", NULL, linha, coluna-yyleng);}
#line 1582 "y.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 89 "jac.y" /* yacc.c:1646  */
    {
                                                                (yyval.node_ptr) = new_node("MethodParams", NULL,(yyvsp[-3].val).linha,(yyvsp[-3].val).coluna); node* params_decl = new_node("ParamDecl", NULL,(yyvsp[-3].val).linha,(yyvsp[-3].val).coluna);
                                                                node* string_array = new_node("StringArray", NULL,(yyvsp[-3].val).linha,(yyvsp[-3].val).coluna); add_child(params_decl,string_array);
                                                                add_child(params_decl,(yyvsp[0].node_ptr)); add_child((yyval.node_ptr), params_decl);
                                                            }
#line 1592 "y.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 94 "jac.y" /* yacc.c:1646  */
    {
                                                                (yyval.node_ptr) = new_node("MethodParams", NULL,(yyvsp[-2].node_ptr)->linha,(yyvsp[-2].node_ptr)->coluna); node* params_decl = new_node("ParamDecl", NULL,(yyvsp[-2].node_ptr)->linha,(yyvsp[-2].node_ptr)->coluna);
                                                                add_child(params_decl,(yyvsp[-2].node_ptr)); add_child(params_decl,(yyvsp[-1].node_ptr)); add_child((yyval.node_ptr), params_decl);
                                                                method_params_shallow_tree((yyval.node_ptr), (yyvsp[0].node_ptr));
                                                            }
#line 1602 "y.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 100 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = NULL;}
#line 1608 "y.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 101 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("FormalParamsRec",NULL, (yyvsp[-3].val).linha,(yyvsp[-3].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr)); add_child((yyval.node_ptr), (yyvsp[-1].node_ptr)); add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1614 "y.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 103 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("VarDecl", NULL, (yyvsp[-3].node_ptr)->linha,(yyvsp[-3].node_ptr)->coluna); add_child((yyval.node_ptr), (yyvsp[-3].node_ptr)); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr)); add_child((yyval.node_ptr), (yyvsp[-1].node_ptr));}
#line 1620 "y.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 105 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = NULL;}
#line 1626 "y.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 106 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("VarDeclRec", NULL,(yyvsp[-1].node_ptr)->linha,(yyvsp[-1].node_ptr)->coluna); add_child((yyval.node_ptr), (yyvsp[-1].node_ptr)); add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1632 "y.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 108 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Bool", NULL, (yyvsp[0].val).linha,(yyvsp[0].val).coluna);}
#line 1638 "y.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 109 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Int", NULL, (yyvsp[0].val).linha,(yyvsp[0].val).coluna);}
#line 1644 "y.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 110 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Double", NULL, (yyvsp[0].val).linha,(yyvsp[0].val).coluna);}
#line 1650 "y.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 112 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Return", NULL, (yyvsp[-1].val).linha,(yyvsp[-1].val).coluna);}
#line 1656 "y.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 113 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Return", NULL, (yyvsp[-2].val).linha,(yyvsp[-2].val).coluna); add_child((yyval.node_ptr),(yyvsp[-1].node_ptr));}
#line 1662 "y.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 114 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = NULL;}
#line 1668 "y.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 115 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = (yyvsp[-1].node_ptr);}
#line 1674 "y.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 116 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = (yyvsp[-1].node_ptr);}
#line 1680 "y.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 117 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = (yyvsp[-1].node_ptr);}
#line 1686 "y.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 118 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Print", NULL, (yyvsp[-4].val).linha,(yyvsp[-4].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));}
#line 1692 "y.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 119 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Print", NULL, (yyvsp[-4].val).linha,(yyvsp[-4].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));}
#line 1698 "y.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 120 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("DoWhile", NULL, (yyvsp[-6].val).linha,(yyvsp[-6].val).coluna); if((yyvsp[-5].node_ptr) == NULL)add_child((yyval.node_ptr), new_node("Block", NULL,(yyvsp[-2].node_ptr)->linha,(yyvsp[-2].node_ptr)->coluna));else add_child((yyval.node_ptr), (yyvsp[-5].node_ptr)); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));}
#line 1704 "y.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 121 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("While", NULL,(yyvsp[-4].val).linha,(yyvsp[-4].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr)); if((yyvsp[0].node_ptr) == NULL)add_child((yyval.node_ptr), new_node("Block", NULL,(yyvsp[-4].val).linha,(yyvsp[-4].val).coluna));else add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1710 "y.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 122 "jac.y" /* yacc.c:1646  */
    {
                                                                (yyval.node_ptr) = new_node("If", NULL,(yyvsp[-6].val).linha,(yyvsp[-6].val).coluna); add_child((yyval.node_ptr), (yyvsp[-4].node_ptr)); if((yyvsp[-2].node_ptr)==NULL) {
                                                                    add_child((yyval.node_ptr), new_node("Block", NULL, 0, 0));
                                                                }
                                                                else
                                                                    add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));
                                                                if((yyvsp[0].node_ptr) == NULL)
                                                                    add_child((yyval.node_ptr), new_node("Block", NULL, 0, 0));
                                                                else
                                                                    add_child((yyval.node_ptr), (yyvsp[0].node_ptr));
                                                            }
#line 1726 "y.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 133 "jac.y" /* yacc.c:1646  */
    {
                                                                (yyval.node_ptr) = new_node("If", NULL,(yyvsp[-4].val).linha,(yyvsp[-4].val).coluna);
                                                                add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));
                                                                if((yyvsp[0].node_ptr)==NULL) {
                                                                    add_child((yyval.node_ptr), new_node("Block", NULL,(yyvsp[-2].node_ptr)->linha,(yyvsp[-2].node_ptr)->coluna));
                                                                }
                                                                else {
                                                                    add_child((yyval.node_ptr), (yyvsp[0].node_ptr));
                                                                }
                                                                add_child((yyval.node_ptr), new_node("Block", NULL, 0, 0));
                                                            }
#line 1742 "y.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 144 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Block", NULL,(yyvsp[-2].val).linha,(yyvsp[-2].val).coluna); statement_shallow_tree((yyval.node_ptr), (yyvsp[-1].node_ptr)); if((yyval.node_ptr)->n_child == 1) {(yyval.node_ptr) = (yyval.node_ptr)->child[0];} else if((yyval.node_ptr)->child == 0){(yyval.node_ptr)=NULL;}}
#line 1748 "y.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 145 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Error", NULL, 0, 0);tree_flag=0;}
#line 1754 "y.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 147 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("StrLit",(yyvsp[0].val).string,(yyvsp[0].val).linha,(yyvsp[0].val).coluna);}
#line 1760 "y.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 149 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = NULL;}
#line 1766 "y.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 150 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("StatementRec", NULL, 0, 0);add_child((yyval.node_ptr), (yyvsp[-1].node_ptr));add_child((yyval.node_ptr),(yyvsp[0].node_ptr));}
#line 1772 "y.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 156 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("BoolLit", (yyvsp[0].val).string,(yyvsp[0].val).linha,(yyvsp[0].val).coluna);}
#line 1778 "y.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 157 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("DecLit", (yyvsp[0].val).string,(yyvsp[0].val).linha,(yyvsp[0].val).coluna);}
#line 1784 "y.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 158 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("RealLit", (yyvsp[0].val).string,(yyvsp[0].val).linha,(yyvsp[0].val).coluna);}
#line 1790 "y.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 159 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = (yyvsp[-1].node_ptr);}
#line 1796 "y.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 160 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Plus",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1802 "y.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 161 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Minus",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1808 "y.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 162 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Not",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1814 "y.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 163 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Add",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1820 "y.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 164 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Sub",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1826 "y.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 165 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Mul",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1832 "y.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 166 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Div",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1838 "y.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 167 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Mod",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1844 "y.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 168 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Eq",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1850 "y.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 169 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Geq",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1856 "y.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 170 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Gt",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1862 "y.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 171 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Leq",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1868 "y.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 172 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Lt",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1874 "y.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 173 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Neq",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1880 "y.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 174 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("And",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1886 "y.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 175 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Or",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr), (yyvsp[-2].node_ptr));add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1892 "y.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 176 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = (yyvsp[0].node_ptr);}
#line 1898 "y.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 177 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Length",NULL,(yyvsp[-1].node_ptr)->linha,(yyvsp[-1].node_ptr)->coluna); add_child((yyval.node_ptr),(yyvsp[-1].node_ptr));}
#line 1904 "y.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 178 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = (yyvsp[0].node_ptr);}
#line 1910 "y.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 179 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = (yyvsp[0].node_ptr);}
#line 1916 "y.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 180 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Error", NULL, 0, 0);tree_flag=0;}
#line 1922 "y.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 182 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("ParseArgs",NULL,(yyvsp[-6].val).linha,(yyvsp[-6].val).coluna); add_child((yyval.node_ptr),(yyvsp[-4].node_ptr)); add_child((yyval.node_ptr),(yyvsp[-2].node_ptr));}
#line 1928 "y.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 183 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Error", NULL, 0, 0);tree_flag=0;}
#line 1934 "y.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 185 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Assign",NULL,(yyvsp[-1].val).linha,(yyvsp[-1].val).coluna); add_child((yyval.node_ptr),(yyvsp[-2].node_ptr)); add_child((yyval.node_ptr),(yyvsp[0].node_ptr));}
#line 1940 "y.tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 187 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Id",(yyvsp[0].val).string,(yyvsp[0].val).linha,(yyvsp[0].val).coluna);}
#line 1946 "y.tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 189 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Void",NULL,(yyvsp[0].val).linha,(yyvsp[0].val).coluna);}
#line 1952 "y.tab.c" /* yacc.c:1646  */
    break;

  case 78:
#line 191 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Call", NULL,(yyvsp[-3].node_ptr)->linha,(yyvsp[-3].node_ptr)->coluna); add_child((yyval.node_ptr), (yyvsp[-3].node_ptr)); call_shallow_tree((yyval.node_ptr), (yyvsp[-1].node_ptr));}
#line 1958 "y.tab.c" /* yacc.c:1646  */
    break;

  case 79:
#line 192 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("Error", NULL,(yyvsp[-3].node_ptr)->linha,(yyvsp[-3].node_ptr)->coluna);tree_flag=0;}
#line 1964 "y.tab.c" /* yacc.c:1646  */
    break;

  case 80:
#line 194 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = NULL;}
#line 1970 "y.tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 195 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("ParamsRec", NULL,(yyvsp[-1].node_ptr)->linha,(yyvsp[-1].node_ptr)->coluna); add_child((yyval.node_ptr),(yyvsp[-1].node_ptr)); add_child((yyval.node_ptr),(yyvsp[0].node_ptr));}
#line 1976 "y.tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 197 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = NULL;}
#line 1982 "y.tab.c" /* yacc.c:1646  */
    break;

  case 83:
#line 198 "jac.y" /* yacc.c:1646  */
    {(yyval.node_ptr) = new_node("ParamsRec", NULL,(yyvsp[-1].node_ptr)->linha,(yyvsp[-1].node_ptr)->coluna); add_child((yyval.node_ptr), (yyvsp[-1].node_ptr)); add_child((yyval.node_ptr), (yyvsp[0].node_ptr));}
#line 1988 "y.tab.c" /* yacc.c:1646  */
    break;


#line 1992 "y.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 200 "jac.y" /* yacc.c:1906  */


int main(int  argc, char *argv[]) {
    class_environment *class_env = NULL;

    if(argc == 2) {
        if(!strcmp(argv[1], "-l")){
            printTokens = 1;
            justLexical = 1;
            table_flag = 0;
            yylex();
        }
        else if(!strcmp(argv[1], "-1")) {
            printTokens = 0;
            justLexical = 1;
            table_flag = 0;
            yylex();
        }
        else if(!strcmp(argv[1], "-t") ) {
            printTokens = 0;
            justLexical = 0;
            tree_flag = 1;
            table_flag = 0;
            yyparse();
        }
        else if(!strcmp(argv[1], "-2")) {
            printTokens = 0;
            justLexical = 0;
            tree_flag = 0;
            table_flag = 0;
            yyparse();
        }
        else if(!strcmp(argv[1], "-s")){
            printTokens = 0;
            justLexical = 0;
            tree_flag = 1;
            table_flag = 1;
            yyparse();
            if(is_error == 0) {
                class_env = check_semantics(root, class_env);
            }
        }
        else if(!strcmp(argv[1], "-3")) {
            printTokens = 0;
            justLexical = 0;
            tree_flag = 0;
            table_flag = 0;
            yyparse();
            if(is_error == 0) {
                class_env = check_semantics(root, class_env);

            }
        }
    }
    else {
        yyparse();
        if(is_error == 0) {
            class_env = check_semantics(root, class_env);
        }
        if(is_error == 0) {
            generate_code(root, class_env);
        }
    }

    yylex_destroy();

    if(table_flag == 1) {
        print_env(class_env);
    }
    if(tree_flag==1) {
        print_tree(root,1);
    }

    free_class_env(class_env);
    free_tree(root);

    return 0;
}
