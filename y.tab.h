/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    STRLIT = 258,
    RESERVED = 259,
    ID = 260,
    BOOLLIT = 261,
    REALLIT = 262,
    DECLIT = 263,
    ASSIGN = 264,
    SEMI = 265,
    COMMA = 266,
    WHILE = 267,
    DO = 268,
    PUBLIC = 269,
    STATIC = 270,
    CLASS = 271,
    PRINT = 272,
    DOTLENGTH = 273,
    PARSEINT = 274,
    RETURN = 275,
    INT = 276,
    BOOL = 277,
    DOUBLE = 278,
    STRING = 279,
    VOID = 280,
    ELSE = 281,
    IF = 282,
    PLUS = 283,
    MINUS = 284,
    STAR = 285,
    DIV = 286,
    MOD = 287,
    GEQ = 288,
    LEQ = 289,
    NEQ = 290,
    EQ = 291,
    LT = 292,
    GT = 293,
    OR = 294,
    AND = 295,
    NOT = 296,
    OSQUARE = 297,
    CSQUARE = 298,
    OBRACE = 299,
    CBRACE = 300,
    OCURV = 301,
    CCURV = 302,
    PRECEDENCE = 303,
    PLUSPREC = 304,
    NO_ELSE = 305
  };
#endif
/* Tokens.  */
#define STRLIT 258
#define RESERVED 259
#define ID 260
#define BOOLLIT 261
#define REALLIT 262
#define DECLIT 263
#define ASSIGN 264
#define SEMI 265
#define COMMA 266
#define WHILE 267
#define DO 268
#define PUBLIC 269
#define STATIC 270
#define CLASS 271
#define PRINT 272
#define DOTLENGTH 273
#define PARSEINT 274
#define RETURN 275
#define INT 276
#define BOOL 277
#define DOUBLE 278
#define STRING 279
#define VOID 280
#define ELSE 281
#define IF 282
#define PLUS 283
#define MINUS 284
#define STAR 285
#define DIV 286
#define MOD 287
#define GEQ 288
#define LEQ 289
#define NEQ 290
#define EQ 291
#define LT 292
#define GT 293
#define OR 294
#define AND 295
#define NOT 296
#define OSQUARE 297
#define CSQUARE 298
#define OBRACE 299
#define CBRACE 300
#define OCURV 301
#define CCURV 302
#define PRECEDENCE 303
#define PLUSPREC 304
#define NO_ELSE 305

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 21 "jac.y" /* yacc.c:1909  */

    struct token_val_ val;
    struct Node* node_ptr;

#line 159 "y.tab.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
